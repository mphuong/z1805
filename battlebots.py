import utils
from utils import KEY2RES


class Bot:
    def __init__(self):
        self.power = 0
        self.sensors = set()
        self.power_slots = []

    def __repr__(self, prefix=''):
        out = f'{prefix}Battlebot\n'
        out += f'{prefix}  Sensors: {self.sensors}\n'
        out += f'{prefix}  Power: {self.power}\n'
        out += f'{prefix}  Power slots:\n'
        for r, power in self.power_slots:
            out += f'{prefix}    {r} --> +{power}\n'
        return out

    def _ask_sensor_shape(self, p):
        missing_sensors = {'circle', 'square', 'triangle'} - self.sensors
        possible_shapes = {shape for shape in missing_sensors
                           if p.breakthroughs.matches([(shape,)])}
        assert possible_shapes, 'Required sensor shapes are missing!'
        print(f'{p.leader}`s breakthroughs: {p.breakthroughs}')
        print('Which sensor to upgrade?')
        mc = utils.MultipleChoice(*possible_shapes)
        return mc.choices[mc.ask()]

    def _upgrade_power(self, p, extra_cost=''):
        print(f'{p.leader}`s resources:')
        print(p.resources.__repr__(prefix='  '))
        print('Choose upgrade:')
        mc = utils.MultipleChoice()
        for r, power in set(self.power_slots):
            if r == 'r' and p.resources[['g', 't', 'u']]:
                mc.add1(f'gold/titanium/uranium --> +{power} power', r, r)
            elif p.resources[r]:
                mc.add1(f'{KEY2RES[r]} --> +{power} power', r, r)
        assert mc.choices, 'Not enough resources!'
        res = mc.ask()
        p.pay(res+extra_cost)

        kv = next((k, v) for k, v in self.power_slots if k == res)
        self.power_slots.remove(kv)
        self.power += kv[1]

    def upgrade_sensor(self, p):
        raise NotImplementedError

    def upgrade_power(self, p):
        raise NotImplementedError


class HarmonyBot(Bot):
    def __init__(self):
        super().__init__()
        self.power_slots = [('n', 4), ('r', 2), ('r', 2), ('r', 2), ('r', 2)]
    
    def upgrade_sensor(self, p):
        w = p.workers.get_active('scientist', 'genius')
        shape = self._ask_sensor_shape(p)
        p.pay(gold=1, bt_options=[(shape,)])
        p.workers.tire(w)

        self.sensors |= {shape}
        self.power += 2

    def upgrade_power(self, p):
        w = p.workers.get_active()
        self._upgrade_power(p)
        p.workers.tire(w)


class DominanceBot(Bot):
    def __init__(self):
        super().__init__()
        self.power = 2
        self.power_slots = [('n', 4), ('t', 2), ('t', 2), ('u', 2), ('u', 2)]
        
    def upgrade_sensor(self, p):
        w = p.workers.get_active('scientist', 'genius')
        shape = self._ask_sensor_shape(p)
        p.pay(gold=1, bt_options=[(shape,)])
        p.workers.tire(w)

        self.sensors |= {shape}
        p.victory_points += 2

    def upgrade_power(self, p):
        w = p.workers.get_active('engineer', 'genius')
        self._upgrade_power(p)
        p.workers.tire(w)
        

class ProgressBot(Bot):
    def __init__(self):
        super().__init__()
        self.power_slots = [('u', 3), ('t', 3), ('t', 3), ('g', 3), ('g', 3)]
    
    def upgrade_sensor(self, p):
        w = p.workers.get_active('scientist', 'genius')
        shape = self._ask_sensor_shape(p)
        p.pay(water=1, bt_options=[(shape,)])
        p.workers.tire(w)

        self.sensors |= {shape}
        p.victory_points += 2

    def upgrade_power(self, p):
        w = p.workers.get_active()
        self._upgrade_power(p)
        p.workers.tire(w)
        

class SalvationBot(Bot):
    def __init__(self):
        super().__init__()
        self.power_slots = [('n', 4), ('n', 4), ('t', 3), ('u', 3)]

    def upgrade_sensor(self, p):
        w = p.workers.get_active()
        shape = self._ask_sensor_shape(p)
        p.pay(gtu=1, bt_options=[(shape,)])
        p.workers.tire(w)

        self.sensors |= {shape}
        p.victory_points += 2

    def upgrade_power(self, p): 
        self._upgrade_power(p, extra_cost='w')

        
