import actions
import buildings
import utils


class Superproject:
    points = 0
    resource_cost = ''
    worker_cost = ''
    breakthrough_cost = None

    action = None
    action_free = False
    # def action(self, player):

    @property
    def action_name(self):
        return 'use_' + utils.camel2snake(self.__class__.__name__)
    
    def instant_effect(self, player):
        pass

    def passive_on(self, player):
        pass

    def passive_off(self, player):
        pass

    def __repr__(self, short=False):
        out = self.__class__.__name__
        if short:
            return out
        out += '\nCost:\n'
        out += utils.longform_res(self.resource_cost, prefix='  ')
        if self.worker_cost:
            out += '  1 x {}\n'.format(self.worker_cost)
        bt_combo_str = []
        for bt_combo in self.breakthrough_cost:
            bt_combo_str += ['[' + ', '.join(bt_combo) + ']']
        out += '  ' + ' or '.join(bt_combo_str)
        return out

    def pay_by(self, p, extra_resources='', resource_discount=''):
        if self.worker_cost:
            n_active = p.workers.active[self.worker_cost]
            n_tired = p.workers.tired[self.worker_cost]
            assert n_active + n_tired >= 1, 'Cannot pay worker cost!'
            
        p.pay(self.resource_cost + extra_resources,
              bt_options=self.breakthrough_cost, res_discount=resource_discount)
        if self.worker_cost:
            p.workers.lose(self.worker_cost)


class AntiGravityField(Superproject):
    points = 6
    resource_cost = 'tttt'
    worker_cost = 'engineer'
    breakthrough_cost = [['triangle_technology'], ['square', 'circle']]

    def passive_on(self, p):
        p._anti_gravity_field = True

    def passive_off(self, p):
        p._anti_gravity_field = False


class ArchiveOfTheEras(Superproject):
    points = 5
    resource_cost = 'nttgg'
    breakthrough_cost = [['square_time'], ['triangle', 'circle']]
        

class CloningVat(Superproject):
    points = 6
    resource_cost = 'uuttwwww'
    breakthrough_cost = [['square', 'square']]

    def action(self, p):
        w = p.workers.get_active()
        p.workers.tire(w)
        p.workers.tired[w] += 1


class ContinuumStabilizer(Superproject):
    points = 6
    resource_cost = 'ttgg'
    breakthrough_cost = [['circle_time'], ['triangle', 'square']]

    def instant_effect(self, p):
        for i in range(3):
            p.rm_warp()


class DarkMatterConverter(Superproject):
    points = 5
    resource_cost = 'ttggww'
    breakthrough_cost = [['square_genetics'], ['triangle', 'square']]

    action_free = True
    def action(self, p):
        n_free = p.workers.active.sum() + p.workers.tired.sum()
        assert n_free, 'Not enough free workers!'
        p.workers.lose()

        print('What to produce?')
        key = utils.MultipleChoice('genius', 'neutronium', 'core').ask()
        if key == 'g':
            p.workers.active.genius += 1
        elif key in ['n', 'c']:
            p.get(key)


class Exocrawler(Superproject):
    points = 6
    resource_cost = 'uuttt'
    breakthrough_cost = [['square_warfare'], ['triangle', 'circle']]

    action_free = True
    def action(self, p):
        assert p.workers.active.sum(), 'No active worker!'
        assert p.resources['exosuits'], 'Not enough exosuits!'
        
        print('Which action to take?')
        mc = utils.MultipleChoiceAbc()
        for ac_name, ac in p.do.__dict__.items():
            if ac_name[0] != '_' and isinstance(ac, actions._BoardAction):
                mc.add(ac_name)
        ac = p.do._actions[mc.ask()]
        ac._call(p)
        ac._disable()


class GrandReservoir(Superproject):
    points = 6
    resource_cost = 'uuttww'
    worker_cost = 'engineer'
    breakthrough_cost = [['circle_society'], ['triangle', 'square']]

    def passive_on(self, p):
        p._grand_reservoir = True

    def passive_off(self, p):
        p._grand_reservoir = False


class NeutroniumResearchCenter(Superproject):
    points = 8
    resource_cost = 'nnttt'
    worker_cost = 'scientist'
    breakthrough_cost = [['square_technology'], ['triangle', 'circle']]

    def instant_effect(self, p):
        print('Research twice.')
        p._game._board_actions['research0']._research(p)
        p._game._board_actions['research0']._research(p)
        

class OutbackConditioner(Superproject):
    points = 4
    resource_cost = 'uuttgg'
    breakthrough_cost = [['circle_genetics'], ['triangle', 'square']]

    def action(self, p):
        print('Which action to take?')
        acstr = utils.MultipleChoiceAbc('research', 'recruit', 'construct').ask()
        ac = p._game._board_actions[acstr+'0']
        
        wcost = ac.water_cost
        xcost = ac.exosuit_cost
        ac.water_cost = 2
        ac.exosuit_cost = 0
        ac._call(p)
        ac.water_cost = wcost
        ac.exosuit_cost = xcost


class ParticleCollider(Superproject):
    points = 7
    resource_cost = 'uttgww'
    breakthrough_cost = [['circle_technology'], ['triangle', 'square']]

    action_free = True
    def action(self, p):
        assert (p.resources['neutronium'] >= 1 or
                p.resources[['g', 't', 'u']] >= 2), 'Not enough resources!'
        
        print('Choose direction:')
        mc = utils.MultipleChoice()
        if p.resources['neutronium'] >= 1:
            mc.add(r='1 x neutronium --> 2 x (gold/titanium/uranium)')
        if p.resources[['g', 't', 'u']] >= 2:
            mc.add(n='2 x (gold/titanium/uranium) --> 1 x neutronium')
        ans = mc.ask()

        if ans == 'r':
            p.pay(neutronium=1)
            p.get(gtu=2)
        elif ans == 'n':
            p.pay(gtu=2)
            p.get(neutronium=1)


class QuantumChameleon(Superproject):
    points = 5
    resource_cost = 'nutg'
    breakthrough_cost = [['circle', 'circle']]

    def action(self, p):
        assert p.workers.active.genius, 'No active genius!'
        bdings = set()
        for player in p._game.players:
            bdings |= player.board.unique()
        bdings = {b for b in bdings if b.action is not None and
                  not b.action_free and not b is self}
        assert bdings, 'There are no buildings with worker actions!'
        print('Which building to use?')
        mc = utils.MultipleChoiceAbc(*{b.__class__.__name__ for b in bdings})
        bname = mc.ask()
        Bding = getattr(buildings, bname, None) or globals()[bname]
        p.workers.set_next('genius')
        Bding().action(p)


class RescuePods(Superproject):
    points = 5
    resource_cost = 'cttwww'
    breakthrough_cost = [['triangle_society'], ['square', 'circle']]

    def passive_on(self, p):
        p._rescue_pods = True

    def passive_off(self, p):
        p._rescue_pods = False


class SyntheticEndorphins(Superproject):
    points = 5
    resource_cost = 'tttwww'
    worker_cost = 'scientist'
    breakthrough_cost = [['triangle_genetics'], ['square', 'circle']]

    def passive_on(self, p):
        p._synthetic_endorphins = True

    def passive_off(self, p):
        p._synthetic_endorphins = False


class TectonicDrill(Superproject):
    points = 6
    resource_cost = 'tttg'
    worker_cost = 'engineer'
    breakthrough_cost = [['circle_warfare'], ['triangle', 'square']]

    def passive_on(self, p):
        p._tectonic_drill = True

    def passive_off(self, p):
        p._tectonic_drill = False


class TemporalTourism(Superproject):
    points = 6
    resource_cost = 'ntttwww'
    breakthrough_cost = [['triangle_time'], ['square', 'circle']]

    action_free = True
    def action(self, p):
        p.jump(3)


class TheUltimatePlan(Superproject):
    points = 3
    resource_cost = 'uttg'
    worker_cost = 'scientist'
    breakthrough_cost = [['triangle', 'triangle']]

    def passive_on(self, p):
        p._ultimate_plan = True

    def passive_off(self, p):
        p._ultimate_plan = False


class UraniumCores(Superproject):
    points = 5
    resource_cost = 'uuut'
    worker_cost = 'scientist'
    breakthrough_cost = [['triangle_warfare'], ['square', 'circle']]

    action_free = True
    def action(self, p):
        p.get(exosuits=1)


class WelfareSociety(Superproject):
    points = 6
    resource_cost = 'ttggwwww'
    breakthrough_cost = [['square_society'], ['triangle', 'circle']]

    def action(self, p):
        w = p.workers.get_active('admin', 'genius')
        p.pay(water=1)
        p.workers.tire(w)
        p.morale_up()
