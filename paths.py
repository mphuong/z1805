import numpy as np
from utils import BREAKTHROUGHS


def player_board_harmony(p):
    p.exosuit_costs = ['', '', '', 'c', 'c/www', 'c/www']
    p.impact_exocosts = ['', 'c/www', 'c/www']
    p.exoslot_water = 2

    p.morale = -1
    p.morale_vp = {-3:-4, -2:-2, -1:0, 0:2, 1:4, 2:6, 3:8}
    p.max_morale_vp = 3
    p.supply_costs = {-3:4, -2:4, -1:5, 0:5, 1:6, 2:7, 3:8}
    
    p.board.costs[1][0] = 'ttgw'
    p.board.costs[2][0] = 'ttw'


def player_board_dominance(p):
    p.exoslot_water = 0
    p.morale_vp = {-3:-3, -2:-2, -1:-1, 0:0, 1:2, 2:4, 3:6}
    p.supply_costs = {-3:4, -2:4, -1:4, 0:4, 1:5, 2:5, 3:5}
    p.time_travel_vp = [2, 4, 6, 8, 10, 12, 12, 12]

    p.board.costs[0][1] = 'nttww'
    p.board.costs[1][2] = 'ttgww'
    p.board.costs[2][1] = 'ttw'


def player_board_progress(p):
    p.exosuit_costs = ['', '', '', 'c/r', 'c/r', 'c/r']
    p.impact_exocosts = ['', 'c/r', 'c/r', 'c/r']
    p.morale_vp = {-3:-6, -2:-4, -1:2, 0:0, 1:3, 2:5, 3:7}
    p.supply_costs = {-3:4, -2:4, -1:5, 0:5, 1:7, 2:8, 3:8}
    p.paradox_limit = 4
    p.time_travel_vp = [1, 2, 4, 8, 10, 12, 14, 16]

    p.board.costs[0][0] = 'nttgw'
    p.board.costs[2][0] = 'utt'


def player_board_salvation(p): 
    p.exosuit_costs = ['', '', 'r', 'r', 'c', 'c']
    p.impact_exocosts = ['', '', 'r', 'c']
    p.morale = 1
    p.morale_vp = {-3:-8, -2:-6, -1:-4, 0:-2, 1:0, 2:2, 3:4}
    p.supply_costs = {-3:4, -2:4, -1:4, 0:5, 1:5, 2:6, 3:7}
    p.time_travel_vp = [2, 4, 6, 8, 12, 14, 16, 20]

    p.board.costs[0][1] = 'nttww'
    p.board.costs[3][2] = 'nttww'


###################################################    
    
def starting_assets_harmony(p):
    p.get(water=6, titanium=3, uranium=1, cores=3)
    p.workers.tired.genius = 1
    p.workers.active.scientist = 2
    p.workers.active.engineer = 2

    
def starting_assets_dominance(p):    
    p.get(water=3, titanium=2, uranium=2, gold=1, cores=4)
    p.workers.tired.engineer = 1
    p.workers.active.scientist = 2
    p.workers.active.engineer = 2


def starting_assets_progress(p):    
    p.get(water=3, titanium=2, gold=1, uranium=1, cores=4)
    p.breakthroughs += [np.random.choice(BREAKTHROUGHS)]
    p.workers.tired.engineer = 1
    p.workers.active.scientist = 2
    p.workers.active.admin = 2

    
def starting_assets_salvation(p):    
    p.get(water=4, titanium=2, gold=2, neutronium=1, cores=3)
    p.workers.tired.admin = 1
    p.workers.active.scientist = 3
    p.workers.active.engineer = 1
