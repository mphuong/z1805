import numpy as np
import utils
from utils import KEY2WORKER


class Building:
    points = 0
    
    def __repr__(self, short=False):
        return self.__class__.__name__

    action = None
    action_free = False
    # def action(self, player):

    def instant_effect(self, player): pass
    def passive_on(self, player): pass
    def passive_off(self, player): pass

    @property
    def action_name(self):
        return 'use_' + utils.camel2snake(self.__class__.__name__)

    
class PowerPlant(Building): pass
class Factory(Building): pass
class LifeSupport(Building): pass
class Lab(Building): pass


class Anomaly:
    def __init__(self, row, col):
        self.row = row
        self.col = col

    def __repr__(self, short=False):
        return 'Anomaly'    
    
    def action(self, p):
        w = p.workers.get_active()
        p.pay('wwn', 'wwrr')
        p.workers.active[w] -= 1
        p.board.rm(self.row, self.col)

    def instant_effect(self, p):
        p.rm_warp()

    def passive_on(self, p): pass
    def passive_off(self, p): pass

    @property
    def action_name(self):
        return f'seal_anomaly{self.row+1}{self.col+1}'


class AnomalyOnBuilding(Anomaly):
    def __init__(self, row, col, bname):
        super().__init__(row, col)
        self.bname = bname

    def action(self, p):
        super().action(p)
        p.board.add(self.bname, (self.row, self.col), trigger_effect=False)

    def instant_effect(self, p):
        super().instant_effect(p)
        b = globals()[self.bname]()
        b.passive_off(p)
        if b.action is not None:
            p.do._rm_action(b.action_name)

    @property
    def action_name(self):
        return 'seal_anomaly_on_' + utils.camel2snake(self.bname)
        
########################

class PowerPlant101(PowerPlant):
    points = 4
    def action(self, p):
        assert p._game.era > 1, 'Cannot time-travel in the first era!'
        w = p.workers.get_active()
        p.workers.tire(w, progress_ability=True)
        p.jump(1)


class PowerPlant102(PowerPlant):
    points = 3
    def action(self, p):
        assert p._game.era > 1, 'Cannot time-travel in the first era!'
        w = p.workers.get_active()
        p.workers.tire(w, progress_ability=True)
        p.jump(2)

class PowerPlant103(PowerPlant102): pass
    

class PowerPlant104(PowerPlant):
    points = 4
    def action(self, p):
        assert p._game.era > 1, 'Cannot time-travel in the first era!'
        w = p.workers.get_active('scientist', 'genius')
        p.workers.tire(w, progress_ability=True)
        p.jump(2)


class PowerPlant105(PowerPlant):
    points = 2
    def action(self, p):
        assert p._game.era > 1, 'Cannot time-travel in the first era!'
        w = p.workers.get_active()
        p.workers.tire(w, progress_ability=True)
        p.jump(3)
    
class PowerPlant106(PowerPlant105): pass


class PowerPlant107(PowerPlant):
    points = 3
    def action(self, p):
        assert p._game.era > 1, 'Cannot time-travel in the first era!'
        w = p.workers.get_active()
        p.pay(uranium=1)
        p.workers.tire(w, progress_ability=True)
        p.jump(3)
        p.victory_points += 1


class PowerPlant108(PowerPlant):
    points = 2
    def action(self, p):
        assert p._game.era > 1, 'Cannot time-travel in the first era!'
        w = p.workers.get_active('scientist', 'genius')
        p.workers.move(w, 'active', 'busy_active')
        p.jump(2)


class PowerPlant109(PowerPlant):
    points = 2
    def action(self, p):
        assert p._game.era > 1, 'Cannot time-travel in the first era!'
        w = p.workers.get_active('scientist', 'genius')
        p.pay(neutronium=1)
        p.workers.tire(w, progress_ability=True)
        p.jump(3)
        p.victory_points += 2


class PowerPlant110(PowerPlant):
    points = 2
    def action(self, p):
        assert p._game.era > 1, 'Cannot time-travel in the first era!'
        w = p.workers.get_active()
        p.pay(water=1)
        p.workers.tire(w, progress_ability=True)
        p.jump(4)

    
class PowerPlant111(PowerPlant):
    points = 1
    def action(self, p):
        assert p._game.era > 1, 'Cannot time-travel in the first era!'
        w = p.workers.get_active()
        p.workers.tire(w, progress_ability=True)
        p.jump(3)

    def instant_effect(self, p):
        p.rm_warp()


class PowerPlant112(PowerPlant):
    points = 1
    def action(self, p):
        assert p._game.era > 1, 'Cannot time-travel in the first era!'
        assert p.resources['water'] > 0, 'Not enough water!'
        w = p.workers.get_active()
        p.workers.tire(w, progress_ability=True)

        n_max = p.resources['water']
        if n_max == 1:
            n = 1
        else:
            print(f'How much water to pay? [1-{n_max}]')
            n = utils.rep_input([str(i) for i in range(1, n_max+1)])
            n = int(n)
        p.pay(water=n)
        p.jump(n)
        p.victory_points += 1
        

class PowerPlant113(PowerPlant):
    points = 1
    def action(self, p):
        assert p._game.era > 1, 'Cannot time-travel in the first era!'
        assert p.resources[['g', 't', 'u']] > 0, \
            'Not enough gold/titanium/uranium!'
        w = p.workers.get_active()
        p.workers.tire(w, progress_ability=True)

        n_max = p.resources[['g', 't', 'u']]
        if n_max == 1:
            n = 1
        else:
            print(f'How much gold/titanium/uranium to pay? [1-{n_max}]')
            n = utils.rep_input([str(i) for i in range(1, n_max+1)])
            n = int(n)
        p.pay(gtu=n)
        p.jump(n)
        p.victory_points += n


class PowerPlant114(PowerPlant):
    points = 1
    def action(self, p):
        assert p._game.era > 1, 'Cannot time-travel in the first era!'
        w = p.workers.get_active('scientist', 'genius')
        p.pay(water=1)
        p.workers.tire(w, progress_ability=True)
        p.jump(3)
        p.jump(3)


class PowerPlant115(PowerPlant):
    points = 3
    def action(self, p):
        assert p._game.era > 1, 'Cannot time-travel in the first era!'
        w = p.workers.get_active()
        p.pay(gold=1)
        p.workers.tire(w, progress_ability=True)
        p.jump(3)
        p.victory_points += 1

##################################################
        
class Factory201(Factory):
    points = 1
    def action(self, p):
        w = p.workers.get_active()
        p.workers.move(w, 'active', 'busy_active')
        p.get(titanium=2)


class Factory202(Factory):
    points = 1
    def action(self, p):
        w = p.workers.get_active()
        p.pay(water=1)
        p.workers.tire(w, progress_ability=True)
        p.get(titanium=3)


class Factory203(Factory):
    points = 2
    def action(self, p):
        w = p.workers.get_active()
        p.pay(water=1)
        p.workers.move(w, 'active', 'busy_active')
        p.get(gtu=1)


class Factory204(Factory):
    points = 3
    def action(self, p):
        w = p.workers.get_active()
        p.workers.move(w, 'active', 'busy_active')
        p.get(gold=1)


class Factory205(Factory):
    points = 2
    def action(self, p):
        w = p.workers.get_active()
        p.pay(water=1)
        p.workers.tire(w, progress_ability=True)
        p.get(gold=2)


class Factory206(Factory):
    points = 3
    def action(self, p):
        w = p.workers.get_active()
        p.workers.move(w, 'active', 'busy_active')
        p.get(uranium=1)


class Factory207(Factory):
    points = 2
    def action(self, p):
        w = p.workers.get_active()
        p.pay(water=1)
        p.workers.tire(w, progress_ability=True)
        p.get(uranium=2)


class Factory208(Factory):
    points = 2
    def action(self, p):
        w = p.workers.get_active()
        p.pay(water=1, gold=1)
        p.workers.tire(w, progress_ability=True)
        p.get(neutronium=1)
        p.victory_points += 1


class Factory209(Factory):
    points = 2
    def action(self, p):
        w = p.workers.get_active()
        p.pay(water=1, uranium=1)
        p.workers.tire(w, progress_ability=True)
        p.get(neutronium=1)
        p.victory_points += 1


class Factory210(Factory):
    points = 1
    def action(self, p):
        w = p.workers.get_active('engineer', 'genius')
        p.pay(water=3)
        p.workers.tire(w, progress_ability=True)
        p.get('n', 'rrr')


class Factory211(Factory):
    points = 3
    def action(self, p):
        w = p.workers.get_active('engineer', 'genius')
        p.pay(titanium=1)
        p.workers.move(w, 'active', 'busy_active')
        p.get(cores=1)
        

class Factory212(Factory):
    points = 2
    def action(self, p):
        w = p.workers.get_active('engineer', 'genius')
        p.workers.tire(w, progress_ability=True)
        p.get(cores=1)


class Factory213(Factory):
    points = 2
    def action(self, p):
        w = p.workers.get_active('engineer', 'genius')
        p.pay(gtu=2)
        p.workers.tire(w, progress_ability=True)
        p.get(cores=2)


class Factory214(Factory):
    points = 1
    def action(self, p):
        w = p.workers.get_active('engineer', 'genius')
        p.pay(water=3)
        p.workers.tire(w, progress_ability=True)
        p.get(cores=2)


class Factory215(Factory):
    points = 1
    action_free = True
    def action(self, p):
        p.pay(water=1)
        p.get(gtu=1)

###########################################

class LifeSupport301(LifeSupport):
    points = 1
    action_free = True
    def action(self, p):
        p.get(water=1)

    def instant_effect(self, p):
        p.get(water=3)

class LifeSupport302(LifeSupport301): pass


class LifeSupport303(LifeSupport):
    points = 1
    action_free = True
    def action(self, p):
        p.get(water=2)
        
class LifeSupport304(LifeSupport303): pass


class LifeSupport305(LifeSupport):
    points = 3
    def action(self, p):
        w = p.workers.get_active()
        p.workers.move(w, 'active', 'busy_active')
        p.get(water=3)

class LifeSupport306(LifeSupport305): pass


class LifeSupport307(LifeSupport):
    points = 2
    def action(self, p):
        w = p.workers.get_active('admin', 'genius')
        p.workers.tire(w)
        p.get(water=5)

class LifeSupport308(LifeSupport307): pass


class LifeSupport309(LifeSupport):
    points = 2
    def action(self, p):
        w = p.workers.get_active()
        p.pay(neutronium=1)
        p.workers.tire(w)
        p.get(water=8)


class LifeSupport310(LifeSupport):
    points = 2
    def action(self, p):
        w = p.workers.get_active()
        p.workers.active[w] -= 1
        p.get(water=7)


class LifeSupport311(LifeSupport):
    points = 1
    def passive_on(self, p):
        p.supply_mult *= 0.5

    def passive_off(self, p):
        p.supply_mult *= 2.0

class LifeSupport312(LifeSupport311): pass


class LifeSupport313(LifeSupport):
    points = 1
    def action(self, p):
        w = p.workers.get_active()
        p.pay(uranium=1)
        p.workers.move(w, 'active', 'busy_active')
        p.get(water=6)
        p.victory_points += 1
        

class LifeSupport314(LifeSupport):
    points = 1
    def action(self, p):
        w = p.workers.get_active()
        p.pay(gold=1)
        p.workers.move(w, 'active', 'busy_active')
        p.get(water=6)
        p.victory_points += 1
        

class LifeSupport315(LifeSupport):
    points = 2
    def instant_effect(self, p):
        p.get(water=8)

##########################################

class Lab401(Lab):
    points = 4
    def passive_on(self, p):
        p.jump_bonus += 1

    def passive_off(self, p):
        p.jump_bonus -= 1


class Lab402(Lab):
    points = 3
    def passive_on(self, p):
        p.jump_bonus += 2

    def passive_off(self, p):
        p.jump_bonus -= 2


class Lab403(Lab):
    points = 2
    def action(self, p):
        w = p.workers.get_active()
        p.pay(cores=1)
        p.workers.move(w, 'active', 'busy_active')
        p.get(exosuits=1)


class Lab404(Lab):
    points = 3
    def action(self, p):
        w = p.workers.get_active('scientist', 'genius')
        p.workers.tire(w, progress_ability=True)
        p.rm_paradox()


class Lab405(Lab):
    points = 3
    def passive_on(self, p):
        p.paradox_limit += 1

    def passive_off(self, p):
        p.paradox_limit -= 1


class Lab406(Lab):
    points = 2
    def passive_on(self, p):
        p.anomaly_vp += 2 

    def passive_off(self, p):
        p.anomaly_vp -= 2
        

class Lab407(Lab):
    points = 2
    def action(self, p):
        w = p.workers.get_active('scientist', 'genius')
        p.workers.tire(w, progress_ability=True)
        p.rm_warp()


class Lab408(Lab):
    points = 2
    def action(self, p):
        w = p.workers.get_active('admin', 'genius')
        p.workers.move(w, 'active', 'busy_active')
        p.workers.refresh()


class Lab409(Lab):
    points = 2
    def action(self, p):
        w = p.workers.get_active('admin', 'genius')
        p.pay(water=2)
        p.workers.move(w, 'active', 'busy_active')

        print('Receive a scientist or an engineer?')
        key = utils.MultipleChoice('scientist', 'engineer').ask()
        p.workers.active[KEY2WORKER[key]] += 1


class Lab410(Lab):
    points = 1
    def action(self, p):
        w = p.workers.get_active('admin', 'genius')
        p.pay(water=2)
        p.workers.move(w, 'active', 'busy_active')
        p.workers.active.genius += 1


class Lab411(Lab):
    points = 2
    def passive_on(self, p):
        p._research_fix2 = True

    def passive_off(self, p):
        p._research_fix2 = False


class Lab412(Lab):
    points = 2
    def action(self, p):
        w = p.workers.get_active()
        p.pay(gtu=1)
        p.workers.tire(w, progress_ability=True)
        p.victory_points += 2


class Lab413(Lab):
    points = 2
    def action(self, p):
        w = p.workers.get_active()
        p.workers.tire(w, progress_ability=True)
        p.get(water=1)
        p.victory_points += 1


class Lab414(Lab):
    points = 1
    action_free = True
    def action(self, p):
        p.victory_points += 2
        p.get_paradox()


class Lab415(Lab):
    points = 2
    def action(self, p):
        w = p.workers.get_active('scientist', 'genius')
        p.workers.active[w] -= 1
        p.get(water=2)
        p.victory_points += 2



