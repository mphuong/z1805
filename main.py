from collections import deque
import copy
import itertools as it
import leaders
import numpy as np
import pandas as pd
from typing import Dict, List, Set, Tuple

import actions, adventures, asset_draft, battlebots, buildings, capital_tiles
import endgame_conds, evacuation, paths, superprojects, utils
from actions import ActionController
from superprojects import Superproject, AntiGravityField, ArchiveOfTheEras
from superprojects import TheUltimatePlan
from utils import N_ERAS, N_HEX_SLOTS, BREAKTHROUGHS, BT_SHAPES
from utils import BUILDING_TYPES, KEY2BTYPES
from utils import LEADERS, KEY2LEADER, COLEADER, LEADER2PATH
from utils import RESOURCES, KEY2RES, WARP_TILES, KEY2WARP
from utils import WORKER_STATES, WORKER_TYPES, KEY2WORKER


class Game:
    def __init__(self):
        self.players = []
        self.era = 0
        self.impact_era = 4
        self._key2leader = copy.deepcopy(KEY2LEADER)

        print('Choose player board side:')
        ab = utils.MultipleChoice(a='identical for all paths',
                                  b='different for each path').ask()
        print('Choose starting assets variant:')
        pd = utils.MultipleChoice('path default', 'draft').ask()
        print('Adventures?')
        yn = utils.MultipleChoice('yes', 'no').ask()
        self.rules = dict(
            player_board_b = (ab == 'b'),
            asset_draft = (pd == 'd'),
            adventures = (yn == 'y'),
        )
        self._init_pools()
            

    def _init_pools(self):
        self.building_stacks = [BuildingStack(b) for b in BUILDING_TYPES]
        self.superprojects = SuperprojectPool(game=self)
        self.mine_pool = MinePool()
        self.recruit_pool = RecruitPool()
        if self.rules['adventures']:
            self.adventures = dict()
            self.adventures['easy'] = AdventureDeck('easy')
            self.adventures['hard'] = AdventureDeck('hard')
        
    def __repr__(self):
        out = ''
        out += f'Era: {self.era}\n\n'
        out += 'Building stacks:\n'
        for stack in self.building_stacks:
            out += f'  {stack}\n'
        if self.era:
            out += '\nMine pool:\n'
            out += f'  {self.mine_pool}\n\n'
            out += 'Recruit pool:\n'
            for w, n in self.recruit_pool.items():
                if n:
                    out += f'  {w} x {n}\n'
        out += '\nSuperprojects:\n'
        out += f'  {self.superprojects}\n\n'
        out += 'Endgame conditions:\n'
        for ec in self.endgame_conditions:
            out += f'  {ec}\n'
        return out

    def add_player(self, leader=None):
        if leader not in self._key2leader.values():
            print('Choose a leader:')
            k = utils.MultipleChoice(**self._key2leader).ask()
            leader = KEY2LEADER[k]
        
        self.players += [Player(self, leader)]
        self._key2leader.pop(LEADERS[leader])
        self._key2leader.pop(LEADERS[COLEADER[leader]])
        return self.players[-1]

    def _fix_endgame_conditions(self):
        print('How to choose endgame conditions?')
        rd = utils.MultipleChoice('randomly', 'draft').ask()
        eg_conds = [getattr(endgame_conds, c)()
                    for c in dir(endgame_conds) if c[0].isupper()
                    and (c != 'MostAdventures' or self.rules['adventures'])]
        if rd == 'r':
            self.endgame_conditions = list(
                np.random.choice(eg_conds, 5, replace=False))
        elif rd == 'd':
            n_hand = {2: 4, 3: 2, 4: 2}[self.n_players]
            n_pick = {2: 2, 3: 1, 4: 1}[self.n_players]
            n_rest = {2: 1, 3: 2, 4: 1}[self.n_players]
            eg_conds = np.random.permutation(eg_conds)
            hands = [list(eg_conds[i*n_hand: (i+1)*n_hand])
                     for i in range(self.n_players)]

            selected = []
            print('\nEndgame condition draft!')
            for i, p in enumerate(self.players):
                print(f'@{p.leader}: Press ENTER to see your hand.')
                input()
                for _ in range(n_pick):
                    print('Choose endgame condition:')
                    mc = utils.MultipleChoiceAbc()
                    for j, ec in enumerate(hands[i]):
                        mc.add1(ec.__repr__(), ret=j)
                    sel = mc.ask()
                    selected += [hands[i][sel]]
                    del hands[i][sel]
                print('\n'*60)

            rest = list(eg_conds[self.n_players*n_hand:])
            rest += [ec for hand in hands for ec in hand]
            random_sel = list(np.random.choice(rest, n_rest, replace=False))
            self.endgame_conditions = selected + random_sel

    def _asset_draft(self):
        asset_cards = np.random.permutation(asset_draft.asset_cards)
        n_each = {2: 8, 3: 5, 4: 4}[self.n_players]
        hands = [list(asset_cards[i*n_each: (i+1)*n_each])
                 for i in range(self.n_players)]
        drawn = [[] for p in self.players]

        print(self)
        print('\nAsset draft!')
        for draft_round in range(4):
            for i, p in enumerate(self.players):
                print(f'@{p.leader}: Press ENTER to see your hand.')
                input()
                if draft_round > 0:
                    print('Previously drawn:')
                    for card in drawn[i]:
                        print(card.__repr__(prefix='  '))

                print('Choose one from:')
                mc = utils.MultipleChoiceAbc()
                for j, card in enumerate(hands[i]):
                    mc.add1('\n'+card.__repr__(prefix='  '), ret=j)
                sel = mc.ask()
                drawn[i] += [hands[i][sel]]
                del hands[i][sel]
                print('\n'*60)
            hands = hands[1:] + [hands[0]]
        self._resolve_asset_draft(drawn)

    def _resolve_asset_draft(self, drawn, verbose=True):
        numbers = [0] * self.n_players
        for i, p in enumerate(self.players):
            if verbose:
                print(f'{p.leader} drew:')
            for card in drawn[i]:
                if verbose:
                    print(card.__repr__(prefix='  '))
                card.receive_assets(p)
                numbers[i] += card.number

        print('Draft sums:')                    
        for p, n in zip(self.players, numbers):
            print(f'  {p.leader:<10} {n}')
        print()

        min_sum = min(numbers)
        argmin_p = [p for i, p in enumerate(self.players)
                    if numbers[i] == min_sum]
        if len(argmin_p) == 1:
            self.set_first_player(argmin_p[0])
        else:
            drawn_min = [min(d) for d in drawn]
            min_n = min(drawn_min)
            argmin_p = [p for i, p in enumerate(self.players)
                        if drawn_min[i] == min_n]
            self.set_first_player(*argmin_p)

    def _setup_board_actions(self):
        self._board_actions = [
            actions.PurifyWater('purify_water'),
            actions.Trade('trade'),
            actions.Research('research0', water_cost=0),
            actions.Research('research1', water_cost=1),
            actions.Recruit('recruit0', water_cost=0),
            actions.Recruit('recruit1', water_cost=1),
            actions.Construct('construct0', water_cost=0),
            actions.Construct('construct1', water_cost=1),
            actions.Mine('mine_gold', 'g'),
            actions.Mine('mine_titanium', 't'),
            actions.Mine('mine_uranium', 'u'),
            actions.WorldCouncil('world_council1', 1, place_banner=False),
            actions.WorldCouncil('world_council2', 2, place_banner=True),
        ]
        if self.n_players == 4:
            self._board_actions += [
                actions.Research('research2', water_cost=2),
                actions.Recruit('recruit2', water_cost=2),
                actions.Construct('construct2', water_cost=2),
            ]
        if self.rules['adventures']:
            self._board_actions += [actions.Adventure('adventure')]
        self._board_actions = {ac._name: ac for ac in self._board_actions}

    def setup(self):
        assert 2 <= self.n_players <= 4
        
        self._fix_endgame_conditions()
        if self.rules['asset_draft']:
            self._asset_draft()
        else:
            self.set_first_player(*self.players)
            for i, p in enumerate(self.players[1:]):
                p.get(water=(1 if i < 2 else 2))

        self._setup_board_actions()
        for p in self.players:
            p.do._add_action(actions.ForceWorkers('force_workers'))
            
            if (self.rules['player_board_b'] and
                LEADER2PATH[p.leader] == 'Dominance'):
                p.do._add_action(actions.SupplyFree('supply'))
            elif (self.rules['player_board_b'] and
                  LEADER2PATH[p.leader] == 'Progress'):
                p.do._add_action(actions.SupplyBusy('supply'))
            else:
                p.do._add_action(actions.Supply('supply'))
                
            if (self.rules['adventures'] and
                LEADER2PATH[p.leader] == 'Salvation'):
                p.do._add_action(actions.PowerUpgradeFree('upgrade_power'))
                p.do._add_action(actions.SensorUpgrade('upgrade_sensor'))
            elif self.rules['adventures']:
                p.do._add_action(actions.PowerUpgrade('upgrade_power'))
                p.do._add_action(actions.SensorUpgrade('upgrade_sensor'))
                
            for action in self._board_actions.values():
                p.do._add_action(action)

    def preparation_phase(self):
        self.era += 1
        for p in self.players:
            p.focus = self.era

        self.superprojects.reveal()
        for bstack in self.building_stacks:
            bstack.shift()
        self.recruit_pool.fill()
        self.mine_pool.fill(self.era > self.impact_era)

    def _n_paradox_rolls(self):
        n_rolls = [0] * self.n_players
        for e in range(1, self.era):
            n_warps = [len(p.warps.in_era(e)) for p in self.players]
            n_max = max(n_warps)
            if n_max:
                for i, p in enumerate(self.players):
                    if n_warps[i] == n_max:
                        n_rolls[i] += 1
        return n_rolls
        
    def paradox_phase(self):
        n_rolls = self._n_paradox_rolls()
        for p, n in zip(self.players, n_rolls):
            p.roll_paradoxes(n)

    def power_up_phase(self):
        payment_options = []
        for p in self.players:
            if p.leader == 'Wolfe':
                print("Wolfe`s resources:")
                print(p.resources.__repr__(prefix='  '))
                print("Use Wolfe`s ability? (1 core <--> 2 water)")
                mc = utils.MultipleChoice(w='core --> water',
                                          c='water --> core', **{'-':'pass'})
                ans = mc.ask()
                if ans == 'w':
                    max_n = p.resources['cores']
                    print(f'How many cores to exchange? [0-{max_n}]')
                    n = utils.rep_input([str(i) for i in range(max_n+1)])
                    p.pay(cores=int(n))
                    p.get(water=2*int(n))
                elif ans == 'c':
                    max_n = p.resources['water'] // 2
                    print(f'How many cores to exchange? [0-{max_n}]')
                    n = utils.rep_input([str(i) for i in range(max_n+1)])
                    p.pay(water=2*int(n))
                    p.get(cores=int(n))                

            print(f'{p.leader}`s resources:')
            print(p.resources.__repr__(prefix='  '))
            exo_costs = [i if i else '-' for i in p.exosuit_costs]
            print(f'{p.leader}`s exosuit slots: [' + ', '.join(exo_costs) + ']')
            
            opt = p.power_up_options()
            m = max(n for n, options in opt.items() if options)
            print(f'@{p.leader}: How many exosuits to power up? [0-{m}]')
            n = int(utils.rep_input([str(i) for i in range(m+1)]))
            payment_options += [(n, opt[n])]
            
        for p, tup in zip(self.players, payment_options):
            n, options = tup
            print(f'@{p.leader}: Powering up {n} exosuits.')
            p.pay(*options)
            n_slots = len(p.exosuit_costs)
            p.get(exosuits=n, water=(n_slots-n)*p.exoslot_water)

    def warp_phase(self):
        warps = []
        for p in self.players:
            w = p.borrow_warps()
            warps += [w]
        for i, p in enumerate(self.players):
            warp_str = ', '.join(KEY2WARP[k] for k in warps[i]) or 'nothing'
            print(f'{p.leader} borrowed {warp_str}.')
            
    def action_phase(self):
        playing = copy.copy(self.players)
        while playing:
            passed = []
            for p in playing:
                if not self._action_round(p):
                    passed += [p]
            for p in passed:
                playing.remove(p)

    def _action_round(self, p) -> bool:
        print(f'@{p.leader}: Choose an action:')
        acs = [ac_name for ac_name in p.do.__dict__ if ac_name[0] != '_']
        ac_name = utils.MultipleChoiceAbc(*sorted(acs), **{'-': 'pass'}).ask()
        if ac_name == 'pass':
            return False
        action = p.do.__getattribute__(ac_name)
        action()
        if isinstance(action, actions._FreeAction):
            return self._action_round(p)
        return True

    def clean_up_phase(self):
        for p in self.players:
            if (p.leader == 'Zaida' and any(self.recruit_pool.values())
                and p.resources['water'] >= 2):
                print('Zaida`s workers:')
                print(p.workers)
                print('@Zaida: Pay 2 water to recruit a worker?')
                mc = utils.MultipleChoice()
                for w, n in self.recruit_pool.items():
                    if n > 0:
                        mc.add(w)
                mc.add(**{'-': 'pass'})
                ans = mc.ask()
                if ans in KEY2WORKER:
                    p.workers.active[KEY2WORKER[ans]] += 1
                    p.pay(water=2)
                    
            elif (p.leader == 'Samira'):
                mc = utils.MultipleChoice()
                mine_pool = self.mine_pool._r.replace('n', '')
                for r in ['gold', 'titanium', 'uranium']:
                    if f'mine_{r}' in p.do.__dict__:
                        mine_pool += r[0]
                    if r[0] in mine_pool:
                        mc.add(r)
                print('Samira`s resources:')
                print(p.resources.__repr__(prefix='  '))
                print('@Samira: Take a resource:')
                r = mc.ask()
                p.get(r)
                if p.resources['water'] >= 2:
                    print(f'Take another resource for 2 water?')
                    if mine_pool.count(r) < 2:
                        del mc.choices[r]
                    mc.add(**{'-': 'pass'})
                    r = mc.ask()
                    if r != '-':
                        p.pay(water=2)
                        p.get(r)

            p.workers.retrieve()
            p.resources['exosuits'] = 0
            p.do._enable_all_actions()

        if self.era == self.impact_era:
            print('Impact!')
            self._impact()
        if self.era == N_ERAS or self._capital_collapsed():
            print('Game end!\n')
            self.end()
        if self.rules['adventures']:
            self._board_actions['adventure'].reset()

    def _capital_collapsed(self):
        for ac_name in self.players[0].do._actions:
            if (ac_name.startswith('construct') or
                ac_name.startswith('recruit') or
                ac_name.startswith('research')):
                return False
        return True

    def _draw_captiles(self):
        captiles = []
        n_captiles = 3 if self.n_players == 4 else 2
        for tile_type in ['Construct', 'Recruit', 'Research']:
            tile_list = [c for c in dir(capital_tiles) if
                         c.startswith(tile_type) and c != tile_type]
            tile_list = np.random.permutation(tile_list)
            for i in range(n_captiles):
                name = utils.camel2snake(tile_list[i])
                captiles += [getattr(capital_tiles, tile_list[i])(name, 0)]
        return captiles
    
    def _impact(self):
        captiles = self._draw_captiles()
        evac_action = actions.Evacuate('evacuate')
        for p in self.players:
            p.exosuit_costs = p.impact_exocosts
            p.do._add_action(evac_action)
            for tile in captiles:
                p.do._add_action(tile)

        for ac_name, ac in self._board_actions.items():
            if (ac_name.startswith('construct') or
                ac_name.startswith('recruit') or
                ac_name.startswith('research')):
                ac._rm()
        for ac in [evac_action] + captiles:
            self._board_actions[ac._name] = ac

    def end(self):
        d = pd.DataFrame(
            index=['Buildings', 'Superprojects', 'Anomalies', 'Time travel',
                   'Morale', 'Breakthroughs', 'VP tokens', 'Outstanding warps']
                  + [ec.__repr__() for ec in self.endgame_conditions],
            columns=[p.leader for p in self.players])
        
        for p in self.players:
            vp = d[p.leader]
            vp['Buildings'] = sum(b.points for b in p.board.buildings)
            vp['Superprojects'] = sum(b.points for b in set(p.board)
                                      if isinstance(b, Superproject))
            if any(isinstance(b, ArchiveOfTheEras) for b in p.board):
                vp['Superprojects'] += p.time_travels
            if any(isinstance(b, TheUltimatePlan) for b in p.board):
                n_sp = sum(isinstance(b, Superproject)
                           for b in p.board.unique())
                vp['Superprojects'] += 3 * n_sp
            
            vp['Anomalies'] = sum(p.anomaly_vp for b in p.board
                                  if isinstance(b, buildings.Anomaly))
            if p.time_travels == 0:
                vp['Time travel'] = 0
            elif p.time_travels > len(p.time_travel_vp):
                vp['Time travel'] = p.time_travel_vp[-1]
            else:
                vp['Time travel'] = p.time_travel_vp[p.time_travels-1]
                
            vp['Morale'] = p.morale_vp[p.morale]
            if vp['Morale'] < 0 and p._synthetic_endorphins:
                vp['Morale'] = 0
            if p._five_beasts and p.morale == p.morale_lims[1]:
                vp['Morale'] += 4
            
            n = p.breakthroughs.n_triples()
            vp['Breakthroughs'] = 2*n + len(p.breakthroughs)
            vp['VP tokens'] = p.victory_points
            vp['Outstanding warps'] = -2 * p.return_all_warps()

        for econd in self.endgame_conditions:
            points = econd.points(self.players)
            for p in self.players:
                d[p.leader][econd.__repr__()] = points[p.leader]

        d.loc['[TOTAL SCORE]'] = d.sum(axis=0)
        print(d)

        scores = []
        for p in self.players:
            scores += [(d[p.leader]['[TOTAL SCORE]'], p.resources['water'],
                        p.resources[['t', 'g', 'u', 'n', 'c']])]
        best_score = sorted(scores)[-1]
        winners = [p.leader for p, sc in zip(self.players, scores)
                   if sc == best_score]
        if len(winners) == 1:
            print(f'\nThe new ruler of the Anachrony council is {winners[0]}!')
        else:
            print('\nThe new rulers of the Anachrony council are {}!'.format(
                ' & '.join(winners)))

    ####################

    def set_first_player(self, *candidates):
        p = np.random.choice(candidates)
        leader = p.leader
        print(f'{leader} is first player.')
        idx = next(i for i, p in enumerate(self.players) if p.leader == leader)
        self.players = self.players[idx:] + self.players[:idx]

    @property
    def n_players(self):
        return len(self.players)
        
    @property
    def timeline(self):
        for e in range(1, self.era+1):
            print(f'Era {e}')
            for p in self.players:
                w = p.warps.in_era(e)
                if w:
                    print('  {}: {}'.format(p.leader, ', '.join(w)))

                
class Player:
    def __init__(self, game, leader):
        assert leader in LEADERS
        self._game = game
        self.leader = leader
        self.focus = 0
        self.victory_points = VictoryPointCounter()
        self.breakthroughs = BreakthroughPool()
        self.resources = ResourcePool()
        self.workers = WorkerPool(self)
        self.board = BuildingBoard(self)
        self.exoslot_water = 1
        self.exosuit_costs = ['', '', '', 'c', 'c', 'c']
        self.impact_exocosts = ['', 'c', 'c', 'c']
        self.morale = 0
        self.morale_lims = (-3, 3)
        self.max_morale_vp = 2
        self.morale_vp = {-3:-6, -2:-4, -1:-2, 0:0, 1:2, 2:4, 3:6}
        self.supply_costs = {-3:4, -2:4, -1:5, 0:5, 1:6, 2:7, 3:7}
        self.supply_mult = 1.0
        self.time_travels = TimeTravelCounter()
        self.time_travel_vp = [2, 4, 6, 8, 10, 12, 14, 16]
        self.paradoxes = 0
        self.paradox_limit = 3
        self.anomaly_vp = -3
        self.warps = WarpPool()
        self.jump_bonus = 0
        self.do = ActionController(self)
        
        self._anti_gravity_field = False
        self._grand_reservoir = False
        self._rescue_pods = False
        self._synthetic_endorphins = False
        self._tectonic_drill = False
        self._ultimate_plan = False
        self._five_beasts = False
        self._old_sewers = False
        self._research_fix2 = False

        path = LEADER2PATH[self.leader]
        if self._game.rules['player_board_b']:
            init_pboard = getattr(paths, 'player_board_'+path.lower())
            init_pboard(self)
            
        if self._game.rules['asset_draft']:
            self.get(water=2, cores=2)
            self.workers.active.scientist = 2
            self.workers.active.engineer = 1
        else:
            init_assets = getattr(paths, 'starting_assets_'+path.lower())
            init_assets(self)

        if self._game.rules['adventures']:
            self.battlebot = getattr(battlebots, f'{path}Bot')()
            self.adventures = []

        if leader == 'Haulani':
            self.do._add_action(leaders.HaulaniAbility('haulani_ability'))
        elif leader == 'Valerian':
            self.do._add_action(leaders.ValerianAbility('valerian_ability'))
        elif leader == 'Cornella':
            self._research_fix2 = True
        elif leader == 'Caratacus':
            self.do._add_action(leaders.CaratacusAbility('caratacus_ability'))
        elif leader == 'Amena':
            self.impact_exocosts = self.exosuit_costs
        self._choose_evacuation_condition()
        
    def _choose_evacuation_condition(self):
        path = LEADER2PATH[self.leader]
        print(f'@{self.leader}: Choose an evacuation condition:')
        evac = dict()
        for i in 'ab':
            evac[i] = getattr(evacuation, f'Evac{path}{i.upper()}')(self)
            print(f'[{i}]')
            print(evac[i].__repr__('    '))
        ab = utils.rep_input(['a', 'b'])
        self.evac_condition = evac[ab]
        
    def __repr__(self):
        out = self.leader + ', {} VP\n'.format(self.victory_points)
        out += '\nResources:\n'
        out += self.resources.__repr__(prefix='  ')
        out += '\nWorkers:\n'
        out += self.workers.__repr__(prefix='  ') 
        out += '\n\nBreakthroughs:\n'
        out += '  ' + self.breakthroughs.__repr__()
        out += '\n\nBoard:\n'
        out += self.board.__repr__(prefix='  ')
        return out

    def get(self, *res_options, **kwargs):
        for k, n in kwargs.items():
            assert n >= 0, f'Amount of {k} must be positive!'

        kw_res = ''
        for k, n in kwargs.items():
            if k in RESOURCES:
                kw_res += RESOURCES[k] * n
            elif k in KEY2RES:
                kw_res += k * n
            elif k == 'gtu':
                kw_res += 'r' * n
            elif k in BREAKTHROUGHS:
                self.breakthroughs += [k] * n
                
        res_opts = []
        for s in res_options or ['']:
            res_opts += utils.r2gtu(kw_res + s)

        if len(res_opts) == 1:
            to_get = res_opts[0]
            res_str = ResourcePool(to_get).to_str()
            desc = utils.longform_res(res_str, prefix='  ')
            if desc:
                print(f'{self.leader} got:\n' + desc[:-1])
        else:
            print('Current resources:')
            print(self.resources.__repr__(prefix='  '))
            print('What to get?')
            mc = utils.MultipleChoiceAbc()
            for s in res_opts:
                desc = ResourcePool(s).__repr__(prefix='  ', show_zeroes=False)
                mc.add1('\n' + desc[:-1], ret=s)
            to_get = mc.ask()
        for k in to_get:
            self.resources[k] += 1

    def pay(self, *res_options, bt_options: List[Tuple[str]] = None,
            res_discount='', enable_reservoir=True, **kwargs):
        # assert breakthroughs
        mandatory_bt = []
        for k, n in kwargs.items():
            if k in BREAKTHROUGHS:
                mandatory_bt += [k] * n
        for bt in set(mandatory_bt):
            del kwargs[bt]

        mandatory_bt = tuple(mandatory_bt)
        bt_options = [tuple(bt_cond) + mandatory_bt
                      for bt_cond in bt_options or [()]]
        bt_opts = self.breakthroughs.matches(bt_options)
        assert bt_opts, 'Not enough breakthroughs!'
            
        # assert resources
        res_core = 'r'*kwargs.pop('gtu', 0) + ResourcePool('', **kwargs).to_str()
        discounted_opts = set()
        for s in res_options or ['']:
            discounted_opts |= utils.apply_discount(s + res_core, res_discount)
        expanded_opts = set()
        for s in discounted_opts:
            expanded_opts |= utils.r2gtu(s)
        
        payment_options = []
        for s in expanded_opts:
            payment = ResourcePool(s)
            if self._grand_reservoir and enable_reservoir and payment['water']:
                payment['water'] -= 1
            if self.resources >= payment:
                payment_options += [payment]
        assert payment_options, 'Not enough resources (or exosuits)!'

        # pay resources
        if len(payment_options) == 1:
            self.resources -= payment_options[0]
            res_str = payment_options[0].to_str()
            desc = utils.longform_res(res_str, prefix='  ')
            if desc:
                print(f'{self.leader} paid:\n' + desc[:-1])
        else:
            print('Current resources:')
            print(self.resources.__repr__(prefix='  '))
            print('What to pay?')
            mc = utils.MultipleChoiceAbc()
            for payment in payment_options:
                payment_str = payment.__repr__(prefix='  ', show_zeroes=False)
                mc.add1('\n' + payment_str[:-1], ret=payment)
            self.resources -= mc.ask()

        # pay breakthroughs
        if len(bt_opts) == 1:
            for bt in bt_opts[0]:
                self.breakthroughs.remove(bt)
        else:
            print('Current breakthroughs:')
            print('  ' + str(self.breakthroughs))
            print('What to pay?')
            mc = utils.MultipleChoiceAbc()
            for bt_tuple in bt_opts:
                mc.add1(', '.join(bt_tuple), ret=bt_tuple)
            for bt in mc.ask():
                self.breakthroughs.remove(bt)

    def pay_max(self, *args, **kwargs):
        assert len(args) < 2
        res_string = args[0] if args else ''
        n_gtu = kwargs.pop('gtu', 0)
        
        to_pay = ResourcePool(res_string, **kwargs).min(self.resources)
        desc = utils.longform_res(to_pay.to_str(), prefix='  ')
        if desc:
            print(f'{self.leader} paid:\n' + desc[:-1])
        for k in KEY2RES:
            self.resources[k] -= to_pay[k]
        self.pay(gtu=min(n_gtu, self.resources[['g', 't', 'u']]))
        
    def morale_up(self):
        print('Morale went up.')
        if self.morale < self.morale_lims[1]:
            self.morale += 1
        else:
            self.victory_points += self.max_morale_vp

    def morale_down(self):
        print('Morale went down.')
        if self.morale > self.morale_lims[0]:
            self.morale -= 1
        elif not self._synthetic_endorphins:
            self.workers.lose()

    def jump(self, n_max, bonus=True):
        assert self._game.era > 1, 'Cannot time-travel in the first era!'
        if bonus:
            n_max = n_max + self.jump_bonus
        focus_min = max(1, self._game.era - n_max)
        print(f'Where to jump? [{focus_min}-{self._game.era-1}]')
        allowed_inputs = [str(i) for i in range(focus_min, self._game.era)]
        self.focus = int(utils.rep_input(allowed_inputs))

        returnable_warps = self._returnable_warps()
        if returnable_warps:
            print('Which warp to return?')
            choices = {WARP_TILES[w]: w for w in returnable_warps}
            choices['-'] = 'none'
            ans = utils.MultipleChoice(**choices).ask()
            if ans in KEY2WARP:
                self._return_warp(KEY2WARP[ans])

    def _return_warp(self, warp):
        if warp in ['admin', 'engineer', 'scientist']:
            self.workers.active[warp] -= 1
        elif warp in ['neutronium', 'gold', 'titanium', 'uranium']:
            self.resources[warp] -= 1
        elif warp == 'exosuit':
            self.resources['exosuits'] -= 1
        elif warp == 'water2':
            self.resources['water'] -= 2
        self.warps[warp] = None
        self.time_travels += 1

    def _returnable_warps(self):
        focussed = self.warps.in_era(self.focus)
        if self.focus == self._game.era or not focussed:
            return []
        out = []
        for w in ['admin', 'engineer', 'scientist']:
            if self.workers.active[w] >= 1:
                out += [w]
        for r in ['neutronium', 'gold', 'titanium', 'uranium']:
            if self.resources[r] >= 1:
                out += [r]
        if self.resources['exosuits'] >= 1:
            out += ['exosuit']
        if self.resources['water'] >= 2:
            out += ['water2']
        return [o for o in out if o in focussed]

    def borrow_warps(self):
        choices = []
        print(f'@{self.leader}: Choose at most two warps:')
        exoslots_full = (self.resources['exosuits'] >= N_HEX_SLOTS)
        for w, era in self.warps.items():
            if era is None and not (w == 'exosuit' and exoslots_full):
                print('[{}] {}'.format(WARP_TILES[w], w))
                choices += [WARP_TILES[w]]
        pairs = [''.join(tup) for tup in it.combinations(choices, 2)]
        pairs = pairs + [s[::-1] for s in pairs]
        combos = [comb for comb in [''] + choices + pairs
                  if self._warps_cost(comb) <= self.resources['water']]
        keys = utils.rep_input(combos)

        if 'w' in keys:
            self.get(water=2)
        for k in keys:
            warp = KEY2WARP[k]
            self.warps[warp] = self._game.era
            if k in 'aes':
                self.workers.active[warp] += 1
                self.pay(water=1)
            elif k in 'ngtux':
                self.get(k)
                
        print('\n'*60)
        return keys

    def _warps_cost(self, warp_combo):
        cost = 0
        for warp in warp_combo:
            if warp in WORKER_TYPES or warp in 'aes':
                cost += 1
            elif warp in ['water2', 'w']:
                cost -= 2
        return max(cost, 0)
                
    def rm_warp(self):
        in_play = self.warps.in_play()
        if not in_play:
            return
        print('Which warp to remove?')
        choices = {WARP_TILES[w]: f'{w} (era {self.warps[w]})' for w in in_play}
        choices['-'] = 'none'
        ans = utils.MultipleChoice(**choices).ask()
        if ans in KEY2WARP:
            self.warps[KEY2WARP[ans]] = None

    def return_all_warps(self) -> int:
        n = 0
        for w in self.warps.in_play():
            if w in WORKER_TYPES:
                if self.workers.active[w] < 1:
                    n += 1
                else:
                    self.workers.active[w] -= 1
                    self.warps[w] = None
            elif w in ['neutronium', 'gold', 'titanium', 'uranium']:
                if self.resources[w] < 1:
                    n += 1
                else:
                    self.resources[w] -= 1
                    self.warps[w] = None
            elif w == 'exosuit':
                if self.resources['exosuits'] < 1:
                    n += 1
                else:
                    self.resources['exosuits'] -= 1
                    self.warps[w] = None
            elif w == 'water2':
                if self.resources['water'] < 2:
                    n += 1
                else:
                    self.resources['water'] -= 2
                    self.warps[w] = None
        return n
        
    def rm_paradox(self):
        if self.paradoxes:
            self.paradoxes -= 1

    def get_paradox(self, n=1):
        print(f'{self.leader} got {n} paradox(es).')
        self.paradoxes += n
        if self.paradoxes >= self.paradox_limit:
            print(f'{self.leader} got an anomaly!')
            self.paradoxes = 0

            lose_building = False
            n_spots = sum(b is None for b in self.board)
            
            if (self._game.rules['player_board_b'] and
                LEADER2PATH[self.leader] == 'Harmony' and self.board.buildings):
                print('Lose a building instead?')
                yn = utils.MultipleChoice('yes', 'no').ask()
                lose_building = (yn == 'y')
            
            if lose_building:
                self.board.lose_building()
            elif n_spots > 0:
                self.board.add_anomaly()
            else:  
                self.board.add_anomaly_on_building()

    def roll_paradoxes(self, n_rolls):
        n_paradoxes = 0
        for i in range(n_rolls):
            n_paradoxes += np.random.choice([0, 1, 1, 1, 1, 2])
        self.get_paradox(n_paradoxes)

    def power_up_options(self) -> Dict[int, Set[str]]: 
        exo_costs = copy.deepcopy(self.exosuit_costs)
        n_free = exo_costs.count('')
        opt = {0: {''}}
        for n in range(1, n_free+1):
            opt[n] = {''}
            exo_costs.remove('')
        for n in range(1, len(exo_costs)+1):
            opt[n_free+n] = self._expand_alts(
                {tuple(sorted(tup)) for tup in it.combinations(exo_costs, n)})
        return opt

    def _expand_alts(self, payment_alts: Set[Tuple[str]]) -> Set[str]:
        out = set()
        for tup in payment_alts:
            out |= {''.join(sorted(newtup)) for newtup in
                    it.product(*[s.split('/') for s in tup])}
        return {s for s in out if self._has_resources(s)}

    def _has_resources(self, s):
        n_gtu = s.count('r')
        try:
            residual = self.resources - ResourcePool(s.replace('r', ''))
            return (residual[['g', 't', 'u']] >= n_gtu)
        except AssertionError:
            return False

    def _mc_possible_buildings(self):
        mc = utils.MultipleChoiceAbc()
        for bstack in self._game.building_stacks:
            if self.board.free_slots(bstack.Type):
                mc.add(*bstack.available())
        return mc


class AdventureDeck:
    def __init__(self, difficulty):
        assert difficulty in ['easy', 'hard']
        adv = [getattr(adventures, a)() for a in dir(adventures)
               if a[0].isupper()]
        if difficulty == 'easy':
            adv = [a for a in adv if a.power < 10]
        elif difficulty == 'hard':
            adv = [a for a in adv if a.power >= 10]
        self._draw = np.random.permutation(adv)
        self._disc = []

    def __iter__(self):
        return iter(list(self._draw) + list(self._disc))

    def discard(self, adv_list):
        self._disc += adv_list

    def draw(self, n):
        drawn = []
        if len(self._draw) < n:
            drawn += list(self._draw)
            n -= len(self._draw)
            self._draw = np.random.permutation(self._disc)
            self._disc = []
        
        drawn += list(self._draw[:n])
        self._draw = self._draw[n:]
        return drawn

    def power_distribution(self):
        powers = [a.power for a in self._draw] + [a.power for a in self._disc]
        d = pd.Series(powers).value_counts()
        d.index.name = 'power'
        d.name = 'count'
        return pd.DataFrame(d.sort_index())

    
class BreakthroughPool(list):
    def __repr__(self):
        return '[' + ', '.join(bt for bt in self) + ']'

    def _trim(self, bt_tuple: Tuple[str], which: Tuple[bool]):
        return tuple(sorted(bt.split('_')[0] if trim else bt
                            for bt, trim in zip(bt_tuple, which)))

    def _satisfied_conds(self, bt_tuple):
        out = set()
        for trim_which in it.product([True, False], repeat=len(bt_tuple)):
            out |= {self._trim(bt_tuple, trim_which)}
        return out

    def matches(self, cond_list):
        cond_list = {tuple(sorted(cond)) for cond in cond_list}
        cond_lens = {len(cond) for cond in cond_list}
        out = []
        for n in cond_lens:
            for bt_tuple in set(it.combinations(self, n)):
                if self._satisfied_conds(bt_tuple) & cond_list:
                    out += [bt_tuple]
        return out

    def n_triples(self):
        n = dict.fromkeys(BT_SHAPES, 0)
        for bt in self:
            n[bt.split('_')[0]] += 1
        return min(n.values())
    
        
class BuildingBoard:
    def __init__(self, player):
        self._player = player
        self._d = [[None for i in range(3)] for j in range(4)]
        self.costs = [['nttw', 'nttgww', 'nnttww'],
                      ['ttg',  'ttgww',  'ttggww'],
                      ['tt',   'uttw',   'uttgww'],
                      ['uttw', 'uttww', 'uuttgww']]
        
        self.power_plants = self._d[0]
        self.factories = self._d[1]
        self.life_supports = self._d[2]
        self.labs = self._d[3]
        self.last = (-1, -1, '')

    def __repr__(self, prefix=''):
        out = ''
        for i in range(4):
            row_strings = []
            for j, building in enumerate(self._d[i]):
                row_strings += [building.__repr__(short=True)
                                if building is not None else self.costs[i][j]]
            out += prefix + '{:<15}'.format(KEY2BTYPES[i+1] + 's')
            out += '[' + ', '.join(row_strings) + ']\n'
        return out

    def __getitem__(self, Btype):
        i_type = BUILDING_TYPES[Btype] - 1
        return self._d[i_type]

    def __iter__(self):
        for i in range(4):
            for j in range(3):
                yield self._d[i][j]

    def _loc(self, bname):
        return next((i, j) for i in range(4) for j in range(3)
                    if self._d[i][j].__repr__() == bname)

    @property
    def buildings(self):
        return [b for b in self if isinstance(b, buildings.Building)]

    def rm(self, row, col):
        if self._d[row][col].action is not None:
            self._player.do._rm_action(self._d[row][col].action_name)
        self._d[row][col].passive_off(self._player)
        self._d[row][col] = None
                
    def unique(self):
        return set(self.__iter__()) - set([None])

    #############
                
    def add(self, bname, loc=None, trigger_effect=True):
        i, j = loc or self.location_new(bname)
        self._d[i][j] = getattr(buildings, bname)()
        self._d[i][j].passive_on(self._player)
        if trigger_effect:
            self._d[i][j].instant_effect(self._player)
        if self._d[i][j].action is not None:
            Action = (actions._FreeAction if self._d[i][j].action_free
                      else actions._Action)
            action = Action(self._d[i][j].action_name)
            action._call = self._d[i][j].action
            self._player.do._add_action(action)
        self.last = (i, j, bname)

    def cost(self, bname):
        i, j = self.location_new(bname)
        return self.costs[i][j]

    def free_slots(self, Type):
        i = BUILDING_TYPES[Type] - 1
        return sum(self._d[i][j] is None for j in range(3))

    def location_new(self, bname):
        Type = bname[:-3]
        i = BUILDING_TYPES[Type] - 1
        assert self.free_slots(Type), \
            'No free building spots for {}s!'.format(Type)
        k = next(j for j in range(3) if self._d[i][j] is None)
        return i, k

    #############

    def add_superproject(self, sp_name):
        if (self._player._game.rules['player_board_b'] and
            LEADER2PATH[self._player.leader] == 'Dominance'):
            row, col = self._location_superproject_dominance()
        else:
            rows, col = self.locations_superproject()
            if len(rows) > 1:
                print(self)
                print('In which row to build superproject?')
                row = utils.MultipleChoice(**{str(r+1): KEY2BTYPES[r+1]
                                              for r in rows}).ask()
                row = int(row) - 1
            else:
                row = rows[0]

        sp = getattr(superprojects, sp_name)()
        self._d[row][col] = self._d[row][col+1] = sp
        sp.instant_effect(self._player)
        sp.passive_on(self._player)
        if sp.action is not None:
            Action = actions._FreeAction if sp.action_free else actions._Action
            action = Action(sp.action_name)
            action._call = sp.action
            self._player.do._add_action(action)
        self.last = (row, col, sp_name)

    def locations_superproject(self):
        sp_loc = [None for i in range(4)]
        for i in range(4):
            if self._d[i][0] is None and self._d[i][1] is None:
                sp_loc[i] = 0
            elif self._d[i][1] is None and self._d[i][2] is None:
                sp_loc[i] = 1
        assert any(loc is not None for loc in sp_loc), \
            'No free building slots for superprojects!'
        min_loc = min(loc for loc in sp_loc if loc is not None)
        return [i for i, loc in enumerate(sp_loc) if loc == min_loc], min_loc

    def _location_superproject_dominance(self):
        sp_loc = [(i, j) for i in range(4) for j in range(2)
                  if self._d[i][j] is None and self._d[i][j+1] is None]
        assert sp_loc, 'No free building slots for superprojects!'

        if len(sp_loc) > 1:
            print(self)
            print('Where to build superproject?')
            mc = utils.MultipleChoice()
            for row, col in sp_loc:
                desc = '{} row, columns {}-{}'.format(KEY2BTYPES[row+1],
                                                      col+1, col+2)
                mc.add1(desc, key=f'{row+1}{col+1}', ret=(row, col))
            return mc.ask()
        else:
            return sp_loc[0]
            
    #############

    def _anomaly_loc(self):
        anom_loc = [None for i in range(4)]
        for i in range(4):
            for j in range(3):
                if self._d[i][j] is None:
                    anom_loc[i] = j
                    break
        min_loc = min(loc for loc in anom_loc if loc is not None)
        return [i for i, loc in enumerate(anom_loc) if loc == min_loc], min_loc

    def _anomaly_loc_salvation(self):
        free_locs = [(i, j) for i in range(4) for j in range(3)
                     if self._d[i][j] is None]
        assert free_locs
        
        if len(free_locs) == 1:
            return free_locs[0]
        else:
            print(self)
            print('Where to put anomaly?')
            mc = utils.MultipleChoice()
            for row, col in free_locs:
                desc = '{} row, column {}'.format(KEY2BTYPES[row+1], col+1)
                mc.add1(desc, key=f'{row+1}{col+1}', ret=(row, col))
            return mc.ask()

    def add_anomaly(self):
        if (self._player._game.rules['player_board_b'] and
            LEADER2PATH[self._player.leader] == 'Salvation'):
            row, col = self._anomaly_loc_salvation()
        else:
            rows, col = self._anomaly_loc()
            if len(rows) > 1:
                print(self)
                print('In which row to put anomaly?')
                row = utils.MultipleChoice(**{str(r+1): KEY2BTYPES[r+1]
                                              for r in rows}).ask()
                row = int(row) - 1
            else:
                row = rows[0]

        self._d[row][col] = buildings.Anomaly(row, col)
        self._d[row][col].instant_effect(self._player)
        action = actions._Action(self._d[row][col].action_name)
        action._call = self._d[row][col].action
        self._player.do._add_action(action)

    def add_anomaly_on_building(self):
        print('On which building to put anomaly?')
        mc = utils.MultipleChoiceAbc(*[b.__repr__() for b in self.buildings])
        bname = mc.ask()

        row, col = self._loc(bname)
        self._d[row][col] = buildings.AnomalyOnBuilding(row, col, bname)
        self._d[row][col].instant_effect(self._player)
        action = actions._Action(self._d[row][col].action_name)
        action._call = self._d[row][col].action
        self._player.do._add_action(action)

    def lose_building(self):
        print('Which building to lose?')
        mc = utils.MultipleChoiceAbc(*[b.__repr__() for b in self.buildings])
        bname = mc.ask()
        self.rm(*self._loc(bname))

    
class BuildingStack:
    def __init__(self, Type):
        self.Type = Type
        self.secondary = deque()
        type_idx = BUILDING_TYPES[Type] * 100
        building_names = [f'{Type}{i}' for i in range(type_idx+1, type_idx+16)]
        self._primary = deque(np.random.permutation(building_names))

    def __repr__(self):
        top_secondary = self.secondary[-1] if self.secondary else '--'
        top = self._primary[-1]
        return '[' + top + ', ' + top_secondary + ']'
        
    def available(self):
        if self.secondary:
            return [self._primary[-1], self.secondary[-1]]
        else:
            return [self._primary[-1]]

    def rm(self, bname):
        if bname == self._primary[-1]:
            self._primary_pop()
        elif bname == self.secondary[-1]:
            self.secondary.pop()

    def shift(self):
        self.secondary.append(self._primary_pop())

    def _primary_pop(self):
        return self._primary.pop()


class MinePool:
    def __init__(self):
        self._deck = deque(np.random.permutation([
            'ggutt', 'gggut', 'nggut', 'nguut', 'nngtt', 'gguut', 
            'nnuuu', 'guuut', 'ngttt', 'guttt', 'ngutt']))

    def __iter__(self):
        return self._r.__iter__()

    def __repr__(self):
        return ', '.join(KEY2RES[r] for r in self._r)

    def fill(self, post_impact=False):
        self._r = self._deck.pop()
        if post_impact:
            self._r = 'n' + self._r[1:]

    def rm(self, r):
        self._r = self._r.replace(r, '', 1)
            
    
class RecruitPool(dict):
    def __init__(self):
        self._deck = deque(np.random.permutation([
            'sseg', 'saag', 'eeag', 'ssag', 'sagg', 'ssee', 
            'seeg', 'seag', 'seaa', 'seaa', 'eegg']))

    def fill(self):
        for w in WORKER_TYPES:
            self[w] = 0
        for k in self._deck.pop():
            self[KEY2WORKER[k]] += 1
            

class ResourcePool(dict):
    def __init__(self, s='', **kwargs):
        for k in kwargs:
            assert k in RESOURCES or k in KEY2RES, f'No such resource: {k}.'
        for k in s:
            assert k in KEY2RES, f'No such resource: {k}.'
            
        for r, resource in KEY2RES.items():
            assert r not in kwargs or resource not in kwargs, \
                f'Keyword argument repeated: {r} / {resource}.'
            self[r] = s.count(r)
            
        kwargs = {(RESOURCES[k] if k in RESOURCES else k): v
                  for k, v in kwargs.items()}
        self.update(kwargs)

    def __repr__(self, prefix='', show_zeroes=True):
        out = ''
        for k, n in self.items():
            if n > 0 or show_zeroes:
                out += f'{prefix}{KEY2RES[k]}: {n}\n'
        return out

    def __getitem__(self, key):
        if isinstance(key, str):
            if key in RESOURCES:
                key = RESOURCES[key]
            return dict.__getitem__(self, key)
        else:
            return sum(dict.__getitem__(self, k) for k in key)

    def __setitem__(self, key, value):
        if key in RESOURCES:
            key = RESOURCES[key]
        super().__setitem__(key, value)

    def __ge__(self, other):
        return all(self[r] >= other[r] for r in KEY2RES)

    def __sub__(self, other):
        for r in self.keys():
            assert self[r] >= other[r], f'Not enough {KEY2RES[r]}.'
        return ResourcePool(**{r: self[r] - other[r] for r in KEY2RES})

    def min(self, res_pool):
        out = ResourcePool()
        for k in KEY2RES:
            out[k] = min(self[k], res_pool[k])
        return out

    def to_str(self):
        out = ''
        for k, n in self.items():
            out += k*n
        return out
    

class SuperprojectPool:
    def __init__(self, game):
        self._game = game
        sp_names = [s for s in dir(superprojects)
                    if s[0].isupper() and s != 'Superproject']
        self._sp = np.random.choice(sp_names, N_ERAS, replace=False)

    def __getitem__(self, era):
        return self._sp[era-1]

    def __iter__(self):
        for i in range(self._game.era + 1):
            yield self._sp[i]

    def __repr__(self):
        out = ['??'] * N_ERAS
        for i in range(self._game.era + 1):
            out[i] = self._sp[i]
        return '[' + ', '.join(out) + ']'

    def reveal(self, era=None):
        pass

    def rm(self, era):
        self._sp[era-1] = '--'

    def rm_superproject(self, sp_name):
        self._sp[self._sp == sp_name] = '--'


class TimeTravelCounter(int):
    def __iadd__(self, n):
        print(f'You went up by {n} on the time travel track.')
        return self + n

        
class VictoryPointCounter(int):
    def __iadd__(self, vp):
        print(f'You got {vp} VP.')
        return self + vp
        

class WarpPool(dict):
    def __init__(self):
        for w in WARP_TILES:
            self[w] = None

    def in_era(self, era=None):
        return {w for w, e in self.items() if e == era}

    def in_play(self):
        return {w for w, e in self.items() if e is not None}

    
class WorkerPool:
    def __init__(self, player, admins=(0, 0), engineers=(0, 0),
                 scientists=(0, 0), geniuses=(0, 0)):
        self._p = player
        self._d = pd.DataFrame(0, index=WORKER_TYPES, columns=WORKER_STATES)
        self._d.iloc[0, [0, 2]] = admins
        self._d.iloc[1, [0, 2]] = engineers
        self._d.iloc[2, [0, 2]] = scientists
        self._d.iloc[3, [0, 2]] = geniuses
        self._letter2row = dict(a=0, e=1, s=2, g=3)
        self._next = None

    def __getattr__(self, key):
        return self._d[key]
        
    def __repr__(self, prefix=''):
        rep = self._d.__repr__()
        return prefix + rep.replace('\n', '\n'+prefix)

    def get_active(self, *types):
        types = WORKER_TYPES if not types else list(types)
        if self._next:
            assert self._next in types, 'Worker not eligible!'
            next_w = self._next
            self._next = None
            return next_w
        
        print(self._d)
        assert self._d.active[types].sum() > 0, 'No eligible workers!'
        print('\nWhich worker should perform the action?')
        mc = utils.MultipleChoice()
        for wt in types:
            if self._d.active[wt] > 0:
                mc.add(wt)
        return KEY2WORKER[mc.ask()]

    def lose(self, *types):
        types = WORKER_TYPES if not types else list(types)
        print(self._d)
        if self._d.active[types].sum() + self._d.tired[types].sum() == 0:
            print('No worker to lose, all are busy!')
            return
        print('\nLose which worker?')
        mc = utils.MultipleChoice()
        for wt in types:
            if self._d.tired[wt] > 0:
                mc.add(f'{wt} (tired)')
            elif self._d.active[wt] > 0:
                mc.add(f'{wt} (active)')
        i = self._letter2row[mc.ask()]
        
        if self._d.tired[i] > 0:
            self._d.tired[i] -= 1
        else:
            self._d.active[i] -= 1
        
    def move(self, wt, from_state, to_state, progress_ability=False):
        assert self._d[from_state][wt] > 0
        self._d[from_state][wt] -= 1
        if (self._p._game.rules['player_board_b'] and 
            LEADER2PATH[self._p.leader] == 'Progress' and wt == 'scientist' and
            progress_ability and to_state == 'busy_tired'):
            to_state = 'busy_active'
        self._d[to_state][wt] += 1

    def n(self, worker_type=None):
        if worker_type is None:
            return self._d.sum().sum()
        return self._d.loc[worker_type].sum()

    def refresh(self):
        self._d.active += self._d.tired
        self._d.tired = 0

    def retrieve(self):
        self._d.active += self._d.busy_active
        self._d.tired += self._d.busy_tired
        self._d.busy_active = 0
        self._d.busy_tired = 0

    def set_next(self, w):
        self._next = w

    def tire(self, wt, progress_ability=False):
        self.move(wt, 'active', 'busy_tired', progress_ability)

######################################


leads = list(LEADERS.keys())
l1 = np.random.choice(leads)
leads.remove(COLEADER[l1])
l2 = np.random.choice(leads)
leads.remove(COLEADER[l2])
l3 = np.random.choice(leads)

g = Game()
p1 = g.add_player(l1)
p2 = g.add_player(l2)
p3 = g.add_player(l3)
g.setup()
g.preparation_phase()
