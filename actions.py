import numpy as np

import superprojects, utils
from utils import BUILDING_TYPES, BT_ICONS, BT_SHAPES, KEY2RES, KEY2WORKER


class ActionController:
    "Exposes available actions as class attributes."
    def __init__(self, player):
        self._player = player
        self._actions = dict()
    
    def __getattribute__(self, key):
        if key[0] != '_':  # accessing action
            self._actions[key]._set_caller(self._player)
        return super().__getattribute__(key)

    def _add_action(self, action):
        if action._name in self._actions:
            return
        self._actions[action._name] = action
        self.__dict__[action._name] = action
        action._add_controller(self)

    def _disable_action(self, name):
        if name in self.__dict__:
            del self.__dict__[name]

    def _enable_all_actions(self):
        self.__dict__.update(self._actions)

    def _rm_action(self, name):
        self._disable_action(name)
        del self._actions[name]


class _Action:
    def __init__(self, name):
        self._name = name
        self._caller = None
        self._controllers = []

    def _add_controller(self, controller):
        if controller in self._controllers:
            return
        self._controllers += [controller]
        controller._add_action(self)
    
    def _set_caller(self, player):
        self._caller = player
        
    def __call__(self):
        self._call(self._caller)
        self._disable()
        self._caller = None

    def _call(self):
        raise NotImplementedError

    def _disable(self):
        for c in self._controllers:
            c._disable_action(self._name)

    def _rm(self):
        for c in self._controllers:
            c._rm_action(self._name)


class _BoardAction(_Action): pass
class _FreeAction(_Action): pass

############################
    
class PurifyWater(_BoardAction):
    def _call(self, p):
        w = p.workers.get_active()
        p.pay(exosuits=1)
        p.workers.tire(w)
        if w in ['scientist', 'genius']:
            p.get(water=4)
        else:
            p.get(water=3)
            
        if p._old_sewers:
            p.get(water=2)

    def _disable(self): pass


class Trade(_BoardAction):
    def _assert(self, p):
        assert p.resources['water'] >= 3 or \
            p.resources['cores'] >= 1 or \
            p.resources['neutronium'] >= 1 or \
            p.resources[['g', 't', 'u']] >= 2, 'Not enough resources!'
    
    def _call(self, p):
        w = p.workers.get_active()
        self._assert(p)
        p.pay(exosuits=1)
        p.workers.tire(w)
        self._trade(p)
        if w in ['admin', 'genius']:
            print('Trade once more? [y, n]')
            yn = utils.MultipleChoice('yes', 'no').ask()
            if yn == 'y':
                self._trade(p)

    def _disable(self): pass

    def _trade(self, p):
        self._assert(p)
        print('Current resources:')
        print(p.resources.__repr__(prefix='  '))
        print('What do you offer?')
        mc = utils.MultipleChoice()
        if p.resources['water'] >= 3:
            mc.add(w='3 x water')
        if p.resources['cores'] >= 1:
            mc.add(c='1 x core')
        if p.resources['neutronium'] >= 1:
            mc.add(n='1 x neutronium')
        if p.resources[['g', 't', 'u']] >= 2:
            mc.add(r='2 x (gold/titanium/uranium)')
        offer = mc.ask()
        
        if offer == 'w':
            p.pay(water=3)
        elif offer == 'c':
            p.pay(cores=1)
        elif offer == 'n':
            p.pay(neutronium=1)
        elif offer == 'r':
            p.pay(gtu=2)

        if offer in 'rc':
            p.get('www', 'n')
        elif offer in 'wn':
            p.get('c', 'rr')


class Research(_BoardAction):
    def __init__(self, name, water_cost, exosuit_cost=1):
        super().__init__(name)
        self.water_cost = water_cost
        self.exosuit_cost = exosuit_cost
    
    def _call(self, p):
        w = p.workers.get_active('scientist', 'genius')
        p.pay(exosuits=self.exosuit_cost, water=self.water_cost)
        self._research(p)
        p.workers.tire(w)

    def _research(self, p, free_fix=False):
        print('Fix shape or icon:')
        mc = utils.MultipleChoice(*BT_SHAPES, **{i[:2]: i for i in BT_ICONS})
        fix = mc.ask()
        
        if len(fix) == 2:  # fixed icon
            icon = mc.choices[fix]
            shape = np.random.choice(BT_SHAPES)
            if free_fix:
                print('Fix shape:')
                mc2 = utils.MultipleChoice(*BT_SHAPES)
                shape = mc2.choices[mc2.ask()]
            elif p._research_fix2 and p.resources['water']:
                print('Fix shape for 1 water?')
                yn = utils.MultipleChoice('yes', 'no').ask()
                if yn == 'y':
                    p.pay(water=1)
                    print('Fix shape:')
                    mc2 = utils.MultipleChoice(*BT_SHAPES)
                    shape = mc2.choices[mc2.ask()]

        elif len(fix) == 1:  # fixed shape
            shape = mc.choices[fix]
            icon = np.random.choice(BT_ICONS + ['wildcard'])
            if free_fix:
                print('Fix icon:')
                mc2 = utils.MultipleChoice(**{i[:2]: i for i in BT_ICONS})
                icon = mc2.choices[mc2.ask()]
            elif p._research_fix2 and p.resources['water']:
                print('Fix icon for 1 water?')
                yn = utils.MultipleChoice('yes', 'no').ask()
                if yn == 'y':
                    p.pay(water=1)
                    print('Fix icon:')
                    mc2 = utils.MultipleChoice(**{i[:2]: i for i in BT_ICONS})
                    icon = mc2.choices[mc2.ask()]
            elif icon == 'wildcard':
                print('You rolled the wildcard! Choose the icon:')
                mc_icon = utils.MultipleChoice(**{i[:2]: i for i in BT_ICONS})
                icon = mc_icon.choices[mc_icon.ask()]
        p.get(**{f'{shape}_{icon}':1})
        print(f'You received {shape}_{icon}.')


class Recruit(_BoardAction):
    def __init__(self, name, water_cost, exosuit_cost=1):
        super().__init__(name)
        self.water_cost = water_cost
        self.exosuit_cost = exosuit_cost

    def _assert_pool(self, p, w):
        pool = p._game.recruit_pool
        assert sum(pool.values()) > 0, 'The recruit pool is empty!'
        if w == 'engineer':
            assert sum(pool.values()) - pool['genius'] > 0, \
                'Engineers cannot recruit geniuses!'

    def _call(self, p):
        w = p.workers.get_active('admin', 'engineer', 'genius')
        self._assert_pool(p, w)
        p.pay(exosuits=self.exosuit_cost, water=self.water_cost)
        self._recruit(p, w)
        p.workers.tire(w)

    def _recruit(self, p, w):
        self._assert_pool(p, w)
        print('Whom to recruit?')
        mc = utils.MultipleChoice()
        for wt in ['admin', 'engineer', 'scientist']:
            if p._game.recruit_pool[wt] > 0:
                mc.add(wt)
        if p._game.recruit_pool['genius'] > 0 and w in ['admin', 'genius']:
            mc.add('genius')
        new_w = KEY2WORKER[mc.ask()]

        p.workers.active[new_w] += 1
        p._game.recruit_pool[new_w] -= 1
        self._get_bonus(p, new_w)

    def _get_bonus(self, p, new_w):
        b = ''
        if new_w == 'genius':
            print('Choose recruit bonus:')
            b = utils.MultipleChoice(w='2 x water', c='1 x core',
                                     v='1 x victory point').ask()
        if new_w == 'admin' or b == 'v':
            p.victory_points +=1
        elif new_w == 'engineer' or b == 'c':
            p.get(cores=1)
        elif new_w == 'scientist' or b == 'w':
            p.get(water=2)
            

class Construct(_BoardAction):
    def __init__(self, name, water_cost, exosuit_cost=1):
        super().__init__(name)
        self.water_cost = water_cost
        self.exosuit_cost = exosuit_cost

    def _call(self, p):
        w = p.workers.get_active('engineer', 'scientist', 'genius')
        self._construct(p, w)
        p.workers.tire(w)

    def _construct(self, p, w):
        print(p.board)
        assert any(b is None for b in p.board), 'No empty building spots!'
        print('What to construct?')
        mc = p._mc_possible_buildings()
        focussed_sp = p._game.superprojects[p.focus]
        if focussed_sp != '--':
            mc.add(s=focussed_sp)
        b = mc.ask()
        
        if b == focussed_sp:
            self._build_superproject(p, w, b)
        else:
            self._build_building(p, w, b)

    def _build_building(self, p, w, bname, discount=''):
        cost = p.board.cost(bname)
        cost += 'x'*self.exosuit_cost + 'w'*self.water_cost
        discount += 't' if w in ['engineer', 'genius'] else ''
        if p._anti_gravity_field:
            discount += 'r'
            
        p.pay(cost, res_discount=discount)
        p.board.add(bname)
        i = BUILDING_TYPES[bname[:-3]] - 1
        p._game.building_stacks[i].rm(bname)

    def _build_superproject(self, p, w, sp_name, discount=''):
        p.board.locations_superproject()
        sp = getattr(superprojects, sp_name)()
        extra_resources = 'x'*self.exosuit_cost + 'w'*self.water_cost
        discount += 't' if w in ['engineer', 'genius'] else ''
        if p._anti_gravity_field:
            discount += 'r'
        
        sp.pay_by(p, extra_resources, discount)
        p.board.add_superproject(sp_name)
        p._game.superprojects.rm(p.focus)


class Mine(_BoardAction):
    def __init__(self, name, resource):
        super().__init__(name)
        self.resource = resource

    def _call(self, p):
        w = p.workers.get_active()
        p.pay(exosuits=1)
        if w in ['engineer', 'genius']:
            p.workers.move(w, 'active', 'busy_active')
        else:
            p.workers.tire(w)

        print('Choose an additional resource:')
        options = set(p._game.mine_pool)
        ans = utils.MultipleChoice(**{k: KEY2RES[k] for k in options}).ask()
        p.get(ans + self.resource)
        p._game.mine_pool.rm(ans)

        if p._tectonic_drill:
            print('Tectonic drill yields yet another resource:')
            tug = utils.MultipleChoice('gold', 'titanium', 'uranium').ask()
            p.get(tug)


class WorldCouncil(_BoardAction):
    def __init__(self, name, water_cost, place_banner):
        super().__init__(name)
        self.water_cost = water_cost
        self.place_banner = place_banner

    def _call(self, p):
        full_actions = []
        if all(not ac_name.startswith('research') for ac_name in p.do.__dict__):
            full_actions += ['research']
        if all(not ac_name.startswith('recruit') for ac_name in p.do.__dict__):
            full_actions += ['recruit']
        if all(not ac_name.startswith('construct') for ac_name in p.do.__dict__):
            full_actions += ['construct']
        assert full_actions or self.place_banner, \
            'All capital actions are available!'

        if full_actions:
            print('Which action to take?')
            ac_name = utils.MultipleChoiceAbc(*full_actions).ask()
            ac = p._game._board_actions[ac_name+'0']

            wcost = ac.water_cost
            ac.water_cost = self.water_cost
            ac._call(p)
            ac.water_cost = wcost
        else:
            p.pay(water=self.water_cost)

        if self.place_banner:
            p._game.set_first_player(p)
        

class Evacuate(_BoardAction):
    def __init__(self, name):
        super().__init__(name)
        self.n_evacuated = 0
    
    def _call(self, p):
        vp = p.evac_condition.score()
        assert vp, 'Evacuation condition not satisfied!'
        
        w = p.workers.get_active()
        p.pay(exosuits=1)
        p.workers.tire(w)

        self.n_evacuated += 1
        if self.n_evacuated == len(p._game.players):
            vp = max(vp-3, 0)
        
        print(f'{p.leader} evacuated for {vp} VP!')
        p.victory_points += vp
        p.do._disable_action(self._name)
        p.do._rm_action(self._name)

    def _disable(self): pass


class Adventure(_BoardAction):
    def __init__(self, name):
        super().__init__(name)
        self.reset()

    def reset(self):
        self._n = 0

    def power_modifier(self):
        if self._n <= 3:
            return 2 - self._n
        return -3

    def _choose_difficulty(self):
        print('Choose difficulty:')
        mc = utils.MultipleChoice('easy', 'hard')
        return mc.choices[mc.ask()]

    def _call(self, p):
        dif = self._choose_difficulty()
        wcost = 1 if dif == 'easy' else 2

        w = p.workers.get_active()
        p.pay(exosuits=1, water=wcost)
        p.workers.tire(w)
        
        self._adventure(p, w, dif)

    def _adventure(self, p, w, difficulty):
        power = p.battlebot.power + self.power_modifier()
        self._n += 1
        
        n_draw = 1 + len(p.battlebot.sensors)
        adv_hand = p._game.adventures[difficulty].draw(n_draw)
        
        print(f'Your power is {power}.')
        print('Which adventure to undertake?')
        mc = utils.MultipleChoiceAbc()
        for a in adv_hand:
            mc.add1(a.__repr__(), ret=a)
        adv = mc.ask()
        
        adv_hand.remove(adv)
        p._game.adventures[difficulty].discard(adv_hand)

        roll = np.random.choice(range(1,7))
        print(f'You rolled +{roll} power!')

        if power + roll >= adv.power:
            print('Your adventure was a Success!')
            adv.success(p, w)
            p.adventures += [adv]
        else:
            print('Your adventure was a failure.')
            adv.failure(p, w)
            p._game.adventures[difficulty].discard([adv])

    def _disable(self): pass

    
class Supply(_Action):
    def _call(self, p):
        w = p.workers.get_active()
        self._supply(p)
        if w in ['admin', 'genius']:
            p.workers.move(w, 'active', 'busy_active')
        else:
            p.workers.tire(w)

    def _supply(self, p):
        water_cost = p.supply_costs[p.morale] * p.supply_mult
        p.pay(water=int(np.ceil(water_cost)))
        p.workers.refresh()
        p.morale_up()


class SupplyBusy(_Action):
    def _call(self, p):
        w = p.workers.get_active()
        Supply._supply(self, p)
        p.workers.tire(w)
        

class SupplyFree(_FreeAction):
    def _call(self, p):
        Supply._supply(self, p)


class ForceWorkers(_FreeAction):
    def _call(self, p):
        p.workers.refresh()
        p.morale_down()


class PowerUpgrade(_Action):
    def _call(self, p):
        p.battlebot.upgrade_power(p)


class PowerUpgradeFree(_FreeAction):
    def _call(self, p):
        p.battlebot.upgrade_power(p)
        

class SensorUpgrade(_Action):
    def _call(self, p):
        p.battlebot.upgrade_sensor(p)
