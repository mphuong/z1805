import copy
import itertools
import re, string
from typing import Dict, List, Set, Tuple


N_ERAS = 7
N_HEX_SLOTS = 6

BT_SHAPES = ['circle', 'triangle', 'square']
BT_ICONS = ['time', 'warfare', 'genetics', 'technology', 'society']
BREAKTHROUGHS = [f'{shape}_{icon}' for shape in BT_SHAPES for icon in BT_ICONS]

BUILDING_TYPES = dict(PowerPlant=1, Factory=2, LifeSupport=3, Lab=4)
KEY2BTYPES = {v: k for k, v in BUILDING_TYPES.items()}

LEADERS = dict(Haulani='h', Zaida='z', Wolfe='w', Samira='s',
               Valerian='v', Cornella='c', Caratacus='k', Amena='a')
KEY2LEADER = {v: k for k, v in LEADERS.items()}
COLEADER = dict(Haulani='Zaida', Zaida='Haulani',
                Wolfe='Samira', Samira='Wolfe',
                Valerian='Cornella', Cornella='Valerian',
                Caratacus='Amena', Amena='Caratacus')
LEADER2PATH = dict(Haulani='Harmony', Zaida='Harmony',
                   Wolfe='Dominance', Samira='Dominance',
                   Valerian='Progress', Cornella='Progress',
                   Caratacus='Salvation', Amena='Salvation')

RESOURCES = dict(water='w', cores='c', neutronium='n', gold='g', titanium='t',
                 uranium='u', exosuits='x')
KEY2RES = {v: k for k, v in RESOURCES.items()}

WARP_TILES = dict(admin='a', engineer='e', scientist='s', neutronium='n',
                  gold='g', titanium='t', uranium='u', exosuit='x', water2='w')
KEY2WARP = {v: k for k, v in WARP_TILES.items()}

WORKER_STATES = ['active', 'busy_active', 'busy_tired', 'tired']
WORKER_TYPES = dict(admin='a', engineer='e', scientist='s', genius='g')
KEY2WORKER = {v: k for k, v in WORKER_TYPES.items()}


def apply_discount(s, discount) -> Set[str]:
    for disc in discount.replace('r', ''):
        if disc in s:
            s = s.replace(disc, '', 1)
        elif disc in 'gtu' and 'r' in s:
            s = s.replace('r', '', 1)
    gtu_discount = discount.count('r')
    s_gtu = 'g'*s.count('g') + 't'*s.count('t') + 'u'*s.count('u')
    s_core = s.replace('g', '').replace('t', '').replace('u', '')
    if len(s_gtu) <= gtu_discount:
        return {s_core.replace('r', '', gtu_discount - len(s_gtu))}
    else:
        n_left = len(s_gtu) - gtu_discount
        return {''.join(sorted(s_core + ''.join(tup)))
                for tup in itertools.combinations(s_gtu, n_left)}


def r2gtu(s) -> Set[str]:
    n = s.count('r')
    s = s.replace('r', '')
    return {''.join(sorted(s+''.join(comb))) for comb in
            itertools.combinations_with_replacement('gtu', n)}

#################################

def camel2snake(s:str):
    return re.sub('([a-z])([A-Z])', r'\1_\2', s).lower()
    

def longform_res(resource_string, prefix=''):
    out = ''
    for k, r in KEY2RES.items():
        if k in resource_string:
            out += '{}{} x {}\n'.format(prefix, resource_string.count(k), r)
    return out

################################

def rep_input(allowed_inputs, prompt=''):
    i = None
    while i not in allowed_inputs:
        i = input(prompt + '> ')
        if i not in allowed_inputs:
            print(f'Allowed inputs: {allowed_inputs}.')
    return i


class MultipleChoice:
    def __init__(self, *args, **kwargs):
        self.choices = kwargs
        self.ret_vals = dict()
        self.add(*args)
    
    def add(self, *args, **kwargs):
        for desc in args:
            self.add1(desc)
        for key, desc in kwargs.items():
            self.add1(desc, key)

    def add1(self, desc, key=None, ret=None):
        key = key if key is not None else desc[0].lower()
        assert key not in self.choices, f'Key "{key}" is taken!'
        self.choices[key] = desc
        if ret is not None:
            self.ret_vals[key] = ret

    def ask(self):
        for key, desc in self.choices.items():
            print(f'[{key}] {desc}')
        key = rep_input(list(self.choices.keys()))
        return self.ret_vals.get(key, key)


class MultipleChoiceAbc:
    def __init__(self, *args, **kwargs):
        self.choices = dict()
        self.ret_vals = dict()
        self._i = 0
        self.add(*args, **kwargs)

    def _next_key(self):
        while True:
            key = string.ascii_lowercase[self._i]
            self._i += 1
            if key not in self.choices:
                return key
        
    def add(self, *args, **kwargs):
        for desc in args:
            self.add1(desc)
        for key, desc in kwargs.items():
            self.add1(desc, key)

    def add1(self, desc, key=None, ret=None):
        key = key if key is not None else self._next_key()
        assert key not in self.choices, f'Key "{key}" is taken!'
        self.choices[key] = desc
        if ret is not None:
            self.ret_vals[key] = ret
            
    def ask(self):
        for key, desc in self.choices.items():
            print(f'[{key}] {desc}')
        key = rep_input(list(self.choices.keys()))
        return self.ret_vals.get(key, self.choices[key])
