import actions
import utils


class CaratacusAbility(actions._FreeAction):
    def _call(self, p):
        print('Choose one:')
        mc = utils.MultipleChoice(w='receive 2 water and a paradox')
        if p.resources['water'] >= 2:
            mc.add(p='pay 2 water and remove a paradox')
        ans = mc.ask()
        
        if ans == 'w':
            p.get(water=2)
            p.get_paradox()
        elif ans == 'p':
            p.pay(water=2)
            p.rm_paradox()


class HaulaniAbility(actions._FreeAction):
    def _call(self, p):
        ac_names = []
        for ac_name in p.do.__dict__:
            if ac_name.startswith('use_') or ac_name == 'supply':
                ac_names += [ac_name]
        assert ac_names, 'No player board actions left!'
        print('Which action to take?')
        ac_name = utils.MultipleChoiceAbc(*ac_names).ask()
        p.do.__getattribute__(ac_name)()


class ValerianAbility(actions._FreeAction):
    def _call(self, p):
        print('Which action to take?')
        mc = utils.MultipleChoiceAbc()
        for ac_name, ac in p.do.__dict__.items():
            if ac_name[0] != '_' and isinstance(ac, actions._BoardAction):
                mc.add(ac_name)
        ac = p.do._actions[mc.ask()]

        p.workers.active.scientist += 1
        p.workers.set_next('scientist')
        try:
            ac._call(p)
            ac._disable()
        except AssertionError as e:
            p.workers.active.scientist -= 1
            raise e
        
        p.workers.busy_tired.scientist -= 1
