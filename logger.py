from collections import deque
import copy, types, string

import ai, actions, asset_draft, capital_tiles, endgame_conds, main
import superprojects, utils
from utils import N_ERAS, BT_ICONS, BT_SHAPES, BUILDING_TYPES
from utils import LEADERS, COLEADER, KEY2LEADER, WORKER_TYPES, KEY2WORKER


class Game(main.Game):
    def _init_pools(self):
        self.building_stacks = [BuildingStack(b) for b in BUILDING_TYPES]
        self.superprojects = SuperprojectPool(game=self)
        self.mine_pool = MinePool()
        self.recruit_pool = RecruitPool()
        if self.rules['adventures']:
            self.adventures = dict()
            self.adventures['easy'] = AdventureDeck('easy')
            self.adventures['hard'] = AdventureDeck('hard')

    def _fix_endgame_conditions(self):
        self.endgame_conditions = []
        eg_conds = [getattr(endgame_conds, c)()
                    for c in dir(endgame_conds) if c[0].isupper()
                    and (c != 'MostAdventures' or self.rules['adventures'])]
        abc = string.ascii_lowercase[:len(eg_conds)]
        eg_conds = dict(zip(abc, eg_conds))
        for i in range(5):
            print(f'Endgame condition {i+1}:')
            k = utils.MultipleChoice(**eg_conds).ask()
            self.endgame_conditions += [eg_conds[k]]
            del eg_conds[k]

    def _asset_draft(self):
        drawn = [[] for p in self.players]
        asset_cards = copy.deepcopy(asset_draft.asset_cards)
        abc = string.ascii_lowercase[:len(asset_cards)]
        asset_cards = dict(zip(abc, asset_cards))
        for i, p in enumerate(self.players):
            for draft_round in range(4):
                print('\n'*10)
                print(f'{p.leader}`s draft {draft_round+1}:')
                mc = utils.MultipleChoice()
                for k, card in asset_cards.items():
                    mc.add1('\n'+card.__repr__(prefix='  '), k, ret=k)
                sel = mc.ask()
                drawn[i] += [asset_cards[sel]]
                del asset_cards[sel]
        self._resolve_asset_draft(drawn, verbose=False)

    def _setup_board_actions(self):
        super()._setup_board_actions()
        for ac_name, ac in self._board_actions.items():
            if ac_name.startswith('research'):
                ac._research = types.MethodType(Research._research, ac)
        if self.rules['adventures']:
            self._board_actions['adventure'] = Adventure('adventure')
        
    def paradox_phase(self):
        n_rolls = self._n_paradox_rolls()
        for p, n in zip(self.players, n_rolls):
            if n:
                print(f'How many paradoxes did {p.leader} roll? [0-{n*2}]')
                n_pdox = int(utils.rep_input([str(i) for i in range(n*2+1)]))
                p.get_paradox(n_pdox)
            else:
                print(f'{p.leader} gets no paradoxes.')

    def _draw_captiles(self):
        captiles = []
        n_captiles = 3 if self.n_players == 4 else 2
        for tile_type in ['Construct', 'Recruit', 'Research']:
            tile_list = [c for c in dir(capital_tiles) if
                         c.startswith(tile_type) and c != tile_type]
            abc = string.ascii_lowercase[:len(tile_list)]
            tiles = dict(zip(abc, tile_list))
            for i in range(n_captiles):
                print(f'{tile_type} capital tile {i+1}:')
                k = utils.MultipleChoice(**tiles).ask()
                name = utils.camel2snake(tiles[k])
                captiles += [getattr(capital_tiles, tiles[k])(name, 0)]
                del tiles[k]

        for ac in captiles:
            if ac._name.startswith('research'):
                ac._research = types.MethodType(Research._research, ac)
        return captiles
    
    def set_first_player(self, *candidates):
        if len(candidates) == 1:
            super().set_first_player(*candidates)
        else:
            print('First player:')
            mc = utils.MultipleChoice()
            for p in candidates:
                mc.add1(p.leader, LEADERS[p.leader], p)
            first_p = mc.ask()
            super().set_first_player(first_p)


class AdventureDeck(main.AdventureDeck):
    def draw(self):
        self._draw = list(self._draw) + list(self._disc)
        self._draw = sorted(self._draw, key=lambda a: (a.power, a.__repr__()))
        
        print('Which adventure was chosen?')
        mc = utils.MultipleChoiceAbc()
        for a in self._draw:
            mc.add1(a.__repr__(), ret=a)
        adv = mc.ask()
        self._draw.remove(adv)
        return adv
    
            
class BuildingStack(main.BuildingStack):
    def __init__(self, Type):
        super().__init__(Type)
        self._unseen = [str(i) for i in range(1, 16)]
        self.set_top()

    def set_top(self):
        type_idx = BUILDING_TYPES[self.Type] * 100
        print(f'{self.Type} primary stack [1-15]:')
        i = int(utils.rep_input(self._unseen))
        self._unseen.remove(str(i))
        bname = f'{self.Type}{type_idx+i}'
        self._primary = deque([bname])

    def _primary_pop(self):
        out = self._primary.pop()
        self.set_top()
        return out


class MinePool(main.MinePool):
    def __init__(self):
        super().__init__()
        self._deck = sorted(self._deck)
    
    def fill(self, post_impact=False):
        print('Mine pool:')
        self._r = utils.MultipleChoiceAbc(*self._deck).ask()
        self._deck.remove(self._r)
        if post_impact:
            self._r = 'n' + self._r[1:]


class RecruitPool(main.RecruitPool):
    def __init__(self):
        super().__init__()
        self._deck = sorted(self._deck)

    def fill(self):
        print('Recruit pool:')
        pool = utils.MultipleChoiceAbc(*self._deck).ask()
        self._deck.remove(pool)
        for w in WORKER_TYPES:
            self[w] = 0
        for k in pool:
            self[KEY2WORKER[k]] += 1

            
class SuperprojectPool(main.SuperprojectPool):
    def __init__(self, game):
        self._game = game
        self._sp = ['??'] * N_ERAS
        self._sp_names = [s for s in dir(superprojects)
                          if s[0].isupper() and s != 'Superproject']
        abc = string.ascii_lowercase[:len(self._sp_names)]
        self._sp_names = dict(zip(abc, self._sp_names))
        self.reveal(era=1)
        
    def reveal(self, era=None):
        era = self._game.era+1 if era is None else era
        print('Superproject, era {}:'.format(era))
        k = utils.MultipleChoice(**self._sp_names).ask()
        self._sp[era-1] = self._sp_names[k]
        del self._sp_names[k]


################################################

class Adventure(actions.Adventure):
    def _adventure(self, p, w, difficulty):
        adv = p._game.adventures[difficulty].draw()
        print('How did it go?')
        sf = utils.MultipleChoice('success', 'failure').ask()
        if sf == 's':
            adv.success(p, w)
            p.adventures += [adv]
        else:
            adv.failure(p, w)
            p._game.adventures[difficulty].discard([adv])            


class Research(actions.Research):
    def _research(self, p, free_fix=False):
        print('Select shape:')
        mc = utils.MultipleChoice(*BT_SHAPES)
        shape = mc.choices[mc.ask()]
        print('Select icon:')
        mc = utils.MultipleChoice(**{i[:2]: i for i in BT_ICONS})
        icon = mc.choices[mc.ask()]
        p.get(**{f'{shape}_{icon}':1})
        
        if p._research_fix2 and p.resources['water']:
            print('Fixed both dice for 1 water?')
            yn = utils.MultipleChoice('yes', 'no').ask()
            if yn == 'y':
                p.pay(water=1)

################################################

g = Game()
red = g.add_player('Amena')
blue = g.add_player('Samira')
green = g.add_player('Zaida')
g.setup()
g.preparation_phase()
