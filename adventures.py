import actions, buildings, superprojects, utils
import numpy as np


def free_building(p, Type):
    i_type = utils.BUILDING_TYPES[Type] - 1
    if any(b is None for b in p.board[Type]):
        print(p.board)
        print('What to construct?')
        mc = utils.MultipleChoiceAbc(
                 *p._game.building_stacks[i_type].available())
        bname = mc.ask()
        p.board.add(bname)
        p._game.building_stacks[i_type].rm(bname)


def optional_trade(p):
    print('Trade with nomads?')
    if utils.MultipleChoice('yes', 'no').ask() == 'y':
        actions.Trade('')._trade(p)


class _Adventure:
    power = np.nan
    def __repr__(self, prefix=''):
        return '{}{}({})'.format(prefix, self.__class__.__name__, self.power)
    
    def success(self, p, w):
        raise NotImplementedError

    def failure(self, p, w):
        raise NotImplementedError

#################################################

class ExosuitMalfunction(_Adventure):
    power = 5
    def success(self, p, w):
        p.get(cores=1, water=3)

    def failure(self, p, w):
        p.pay_max(cores=1)
        p.get(water=1)


class MysticalTeachings(_Adventure):
    power = 5
    def success(self, p, w):
        print('You got a genius.')
        p.workers.active.genius += 1
        p.get(water=3)

    def failure(self, p, w):
        p.workers.lose()
        print('You got a genius.')
        p.workers.active.genius += 1


class RaidOnNomadVillage(_Adventure):
    power = 5
    def success(self, p, w):
        p.get(gtu=1)
        p.workers.active.admin += 1
        print('You got an admin.')

    def failure(self, p, w):
        p.workers.lose()
        p.get(gtu=1)


class DataArchives(_Adventure):
    power = 6
    def success(self, p, w):
        ac = p._game._board_actions['research0']
        ac._research(p, free_fix=True)
        p.victory_points += 1

    def failure(self, p, w):
        p.pay_max(titanium=1)
        ac = p._game._board_actions['research0']        
        ac._research(p, w)


class JourneyThroughTheRift(_Adventure):
    power = 6
    def success(self, p, w):
        print('Your paradox limit increased by 1.')
        p.paradox_limit += 1

    def failure(self, p, w):
        p.get_paradox()
        p.victory_points += 1


class NuclearWinter(_Adventure):
    power = 6
    def success(self, p, w):
        p.get(water=6)
        p.victory_points += 1

    def failure(self, p, w):
        p.workers.lose()
        p.morale_up()


class AncientGoldMine(_Adventure):
    power = 7
    def success(self, p, w):
        p.get(gold=2)
        p.victory_points += 2

    def failure(self, p, w):
        p.pay_max(water=2)
        p.get(gold=1)
        p.victory_points += 1


class ForgottenTimeCapsule(_Adventure):
    power = 7
    def success(self, p, w):
        p.victory_points += 3

    def failure(self, p, w):
        p.pay_max(water=2)
        p.victory_points += 1


class IrradiatedVerminTide(_Adventure):
    power = 7
    def success(self, p, w):
        p.get(uranium=2)
        p.victory_points += 2

    def failure(self, p, w):
        p.pay_max(cores=1)
        p.get(uranium=1)
        p.victory_points += 1


class PassageOfTheFiveBeasts(_Adventure):
    power = 7
    def success(self, p, w):
        p._five_beasts = True
        print('At the end of the game, the highest space on the'
              ' Morale track is worth 4 additional VP for you.')
        
    def failure(self, p, w):
        p.morale_down()
        p.victory_points += 2


class ElectromagneticHurricane(_Adventure):
    power = 8
    def success(self, p, w):
        p.get(cores=2)
        p.victory_points += 1

    def failure(self, p, w):
        p.pay_max(water=2)
        p.victory_points += 1


class SecretTunnels(_Adventure):
    power = 8
    def _take_action(self, p, w):
        print('Which action to take?')
        mc = utils.MultipleChoiceAbc('construct', 'recruit', 'research')
        ac_name = mc.ask()
        ac = p._game._board_actions[ac_name+'0']

        if ac_name == 'construct':
            ac._construct(p, w)
        elif ac_name == 'recruit':
            ac._recruit(p, w)
        elif ac_name == 'research':
            ac._research(p)
    
    def success(self, p, w):
        print('You may take two actions.')
        self._take_action(p, w)
        print('You have another action.')
        self._take_action(p, w)

    def failure(self, p, w):
        p.pay_max(water=2)
        self._take_action(p, w)


class TemporalCrack(_Adventure):
    power = 8
    def success(self, p, w):
        p.rm_warp()
        p.victory_points += 2

    def failure(self, p, w):
        p.get_paradox()
        p.victory_points += 1


class TribesOfTheOutback(_Adventure):
    power = 8
    def _action(p):
        actions.Trade('')._trade(p)
    
    def success(self, p, w):
        action = actions._FreeAction('use_outback_tribes')
        action._call = TribesOfTheOutback._action
        p.do._add_action(action)
        print('You got the action: use_outback_tribes.')

    def failure(self, p, w):
        p.pay_max(water=1)
        optional_trade(p)


class CargoShipWreckage(_Adventure):
    power = 9
    def success(self, p, w):
        p.get(titanium=1, gold=1, uranium=1)
        p.victory_points += 1

    def failure(self, p, w):
        p.pay_max(gtu=1)
        p.victory_points += 1


class GiantSandworm(_Adventure):
    power = 9
    def success(self, p, w):
        p.battlebot.power += 4
        print('Your battlebot`s power went up by 4.')

    def failure(self, p, w):
        p.pay_max(cores=1)
        p.victory_points += 1


class HiddenResourceStorage(_Adventure):
    power = 9
    def _action(p):
        p.pay(water=1)
        p.get(gtu=1)
    
    def success(self, p, w):
        action = actions._FreeAction('use_resource_storage')
        action._call = HiddenResourceStorage._action
        p.do._add_action(action)
        print('You got the action: use_resource_storage.')

    def failure(self, p, w):
        p.pay_max(gtu=1)
        p.victory_points += 1


class OldSewers(_Adventure):
    power = 9
    def success(self, p, w):
        p._old_sewers = True
        print('For the remainder of the game, you receive '
              '2 additional W when you take the Purify Water Action.')

    def failure(self, p, w):
        p.pay_max(titanium=1)
        p.get(water=2)


class HostileNomads(_Adventure):
    power = 10
    def success(self, p, w):
        p.get(water=6)
        p.victory_points += 2
        optional_trade(p)
        optional_trade(p)

    def failure(self, p, w):
        p.workers.lose()
        p.victory_points += 1


class NeutroniumCave(_Adventure):
    power = 10
    def success(self, p, w):
        p.get(neutronium=2)
        p.victory_points += 1

    def failure(self, p, w):
        p.get_paradox(2)
        p.get(neutronium=1)


class TheTimeMaker(_Adventure):
    power = 10
    def success(self, p, w):
        p.time_travels += 2

    def failure(self, p, w):
        p.get_paradox()
        p.victory_points += 1


class BroadcastFromThePast(_Adventure):
    power = 11
    def success(self, p, w):
        p.morale_up()
        p.morale_up()

    def failure(self, p, w):
        p.morale_down()
        p.victory_points += 2


class FountainOfLife(_Adventure):
    power = 11
    def success(self, p, w):
        p.get(water=8)
        p.victory_points += 2

    def failure(self, p, w):
        p.workers.lose()
        p.get(uranium=1)


class AbandonedFactory(_Adventure):
    power = 12
    def success(self, p, w):
        free_building(p, 'Factory')
        
    def failure(self, p, w):
        p.pay_max(cores=1)
        p.get(titanium=2)


class UncontaminatedReservoir(_Adventure):
    power = 12
    def success(self, p, w):
        p.victory_points += 1
        free_building(p, 'LifeSupport')

    def failure(self, p, w):
        p.workers.lose()
        p.get(water=4)


class UndergroundLaboratory(_Adventure):
    power = 12
    def success(self, p, w):
        free_building(p, 'Lab')

    def failure(self, p, w):
        p.morale_down()
        p.workers.active.scientist += 1


class AncientTempleRuins(_Adventure):
    power = 13
    def success(self, p, w):
        p.get(gold=4)
        p.victory_points += 2

    def failure(self, p, w):
        p.workers.lose()
        p.morale_up()


class DesertedCarrier(_Adventure):
    power = 13
    def _action(p):
        p.get(exosuits=1)
    
    def success(self, p, w):
        action = actions._FreeAction('use_deserted_carrier')
        action._call = DesertedCarrier._action
        p.do._add_action(action)
        print('You got the action: use_deserted_carrier.')

    def failure(self, p, w):
        p.pay_max(cores=1)
        p.get(gtu=1)


class IceboundCryochamber(_Adventure):
    power = 13
    def success(self, p, w):
        actions.Recruit('', 0, 0)._recruit(p, w)
        p.workers.active.genius += 1
        print('You got a genius.')
        p.victory_points += 2

    def failure(self, p, w):
        p.workers.lose()
        p.get(water=2)
        p.victory_points += 1


class AdrenalineShots(_Adventure):
    power = 14
    def success(self, p, w):
        p.get(water=3)
        for i in range(1, 3):
            print(f'Take an adventure action? ({i}/2)')
            yn = utils.MultipleChoice('yes', 'no').ask()
            if yn == 'y':
                ac = p._game._board_actions['adventure']
                dif = ac._choose_difficulty()
                wcost = 1 if dif == 'easy' else 2
                p.pay(water=wcost)
                ac._adventure(p, w, dif)

    def failure(self, p, w):
        p.workers.lose()
        p.get(cores=1)


class RogueAi(_Adventure):
    power = 14
    def success(self, p, w):
        ac = p._game._board_actions['research0']
        ac._research(p, free_fix=True)
        ac._research(p, free_fix=True)

    def failure(self, p, w):
        print('You lost a breakthrough.')
        p.pay(bt_options=[(bt,) for bt in p.breakthroughs])
        p.victory_points += 2


class AsteroidDebris(_Adventure):
    power = 15
    def success(self, p, w):
        p.get(neutronium=1, titanium=1, uranium=1, gold=1)
        p.victory_points += 1

    def failure(self, p, w):
        p.workers.lose()
        p.get(gtu=2)


class TrailsToTheLostCity(_Adventure):
    power = 15
    def _action(p):
        p.pay(water=1)
        p.morale_up()
    
    def success(self, p, w):
        action = actions._FreeAction('use_lost_city')
        action._call = TrailsToTheLostCity._action
        p.do._add_action(action)
        print('You got the action: use_lost_city.')

    def failure(self, p, w):
        p.pay_max(water=2)
        p.workers.lose()
        p.morale_up()


class SecretMilitaryFacility(_Adventure):
    power = 16
    def success(self, p, w):
        p.get(cores=3)
        p.victory_points += 3

    def failure(self, p, w):
        p.pay_max(cores=1)
        p.victory_points += 2


class UnstableNeutroniumCore(_Adventure):
    power = 16
    def success(self, p, w):
        free_building(p, 'PowerPlant')
        anomalies = [b for b in p.board if isinstance(b, buildings.Anomaly)]
        if anomalies:
            print(p.board)
            print('Which anomaly to remove?')
            mc = utils.MultipleChoiceAbc()
            for b in anomalies:
                desc = '{} row, column {}'.format(
                    utils.KEY2BTYPES[b.row+1], b.col+1)
                mc.add1(desc, key=f'{b.row+1}{b.col+1}', ret=b)
            b = mc.ask()
            p.board.rm(b.row, b.col)
            if isinstance(b, buildings.AnomalyOnBuilding):
                p.board.add(b.bname, (b.row, b.col), trigger_effect=False)

    def failure(self, p, w):
        p.workers.lose()
        p.get(neutronium=1)


class MetropolisRuins(_Adventure):
    power = 18
    def success(self, p, w):
        print(p.board)
        assert any(b is None for b in p.board), 'No empty building spots!'
        try:
            sp_possible = p.board.locations_superproject()
        except AssertionError:
            sp_possible = False

        mc = p._mc_possible_buildings()
        if sp_possible:
            print('You may construct a superproject or two buildings.')
            mc.add(*[sp for sp in p._game.superprojects if sp != '--'])
        else:
            print('You may construct two buildings.')
        print('What to construct?')
        b = mc.ask()

        if b in dir(superprojects):
            p.board.add_superproject(b)
            p._game.superprojects.rm_superproject(b)
        else:
            p.board.add(b)
            i = utils.BUILDING_TYPES[b[:-3]] - 1
            p._game.building_stacks[i].rm(b)

            if any(b is None for b in p.board):
                print(p.board)
                print('You may construct another building. Which one?')
                b = p._mc_possible_buildings().ask()
                p.board.add(b)
                i = utils.BUILDING_TYPES[b[:-3]] - 1
                p._game.building_stacks[i].rm(b)

    def failure(self, p, w):
        p.pay_max(cores=1)
        p.workers.lose()
        p.victory_points += 2
