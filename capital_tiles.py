from actions import Construct, Recruit, Research
import superprojects, utils
from utils import BT_ICONS, BT_SHAPES


class CollapsingConstruct(Construct):
    def _disable(self):
        self._rm()

        
class CollapsingRecruit(Recruit):
    def _disable(self):
        self._rm()

        
class CollapsingResearch(Research):
    def _disable(self):
        self._rm()

###############################################

class ConstructDiscountGtu(CollapsingConstruct):
    def _build_building(self, p, w, bname):
        super()._build_building(p, w, bname, discount='r')

    def _build_superproject(self, p, w, sp_name):
        super()._build_superproject(p, w, sp_name, discount='r')


class ConstructDiscountN(CollapsingConstruct):
    def _build_building(self, p, w, bname):
        super()._build_building(p, w, bname, discount='n')

    def _build_superproject(self, p, w, sp_name):
        super()._build_superproject(p, w, sp_name, discount='n')


class ConstructBuildingVp(CollapsingConstruct):
    def _build_building(self, p, w, bname):
        super()._build_building(p, w, bname)
        row, col, bname = p.board.last
        p.victory_points += col + 1

        
class ConstructSuperprojectVp(CollapsingConstruct):
    def _build_superproject(self, p, w, sp_name):
        super()._build_superproject(p, w, sp_name)
        p.victory_points += 2

        
class ConstructTwice(CollapsingConstruct):
    def _call(self, p):
        w = p.workers.get_active('engineer', 'scientist', 'genius')
        self._construct(p, w)
        print('You may construct once more.')
        self.exosuit_cost = self.water_cost = 0
        self._construct(p, w)
        p.workers.tire(w)

###############################################

class RecruitDoubleBonus(CollapsingRecruit):
    def _get_bonus(self, p, new_w):
        b = ''
        if new_w == 'genius':
            print('Choose recruit bonus:')
            b = utils.MultipleChoice(cc='2 cores', vv='2 VPs', ww='4 water',
                                     cv='1 core + 1 VP', cw='1 core + 2 water',
                                     vw='1 VP + 2 water').ask()
        if new_w == 'admin' or b == 'vv':
            p.victory_points += 2
        elif new_w == 'engineer' or b == 'cc':
            p.get(cores=2)
        elif new_w == 'scientist' or b == 'ww':
            p.get(water=4)
        elif b == 'cv':
            p.get(cores=1)
            p.victory_points += 1
        elif b == 'cw':
            p.get(cores=1, water=2)
        elif b == 'vw':
            p.get(water=2)
            p.victory_points += 1


class RecruitExosuit(CollapsingRecruit):
    def _call(self, p):
        super()._call(p)
        p.get(exosuits=1)


class RecruitMorale(CollapsingRecruit):
    def _call(self, p):
        super()._call(p)
        p.morale_up()


class RecruitRefreshWorkers(CollapsingRecruit):
    def _call(self, p):
        super()._call(p)
        p.workers.refresh()


class RecruitTwice(CollapsingRecruit):
    def _call(self, p):
        w = p.workers.get_active('admin', 'engineer', 'genius')
        assert sum(p._game.recruit_pool.values()) > 0, \
            'The recruit pool is empty!'
        p.pay(exosuits=self.exosuit_cost, water=self.water_cost)
        self._recruit(p, w)
        
        if sum(p._game.recruit_pool.values()) > 0:
            print('You may recruit once more.')
            self._recruit(p, w)
        p.workers.tire(w)
        

###############################################

class ResearchFixOutcome(CollapsingResearch):
    def _research(self, p):
        print('Fix shape:')
        mc = utils.MultipleChoice(*BT_SHAPES)
        shape = mc.choices[mc.ask()]
        print('Fix icon:')
        mc = utils.MultipleChoice(**{i[:2]: i for i in BT_ICONS})
        icon = mc.choices[mc.ask()]
        
        p.get(**{f'{shape}_{icon}':1})
        print(f'You received {shape}_{icon}.')

        
class Research2Vp(CollapsingResearch):
    def _call(self, p):
        super()._call(p)
        p.victory_points += 2

        
class ResearchThenConstruct(CollapsingResearch):
    def _call(self, p):
        w = p.workers.get_active('scientist', 'genius')
        p.pay(exosuits=self.exosuit_cost, water=self.water_cost)
        self._research(p)

        focussed_sp = p._game.superprojects[p.focus]
        if focussed_sp != '--':
            print(getattr(superprojects, focussed_sp)())
            print(f'Construct {focussed_sp}?')
            yn = utils.MultipleChoice('yes', 'no').ask()
            if yn == 'y':
                construct = Construct('', water_cost=0, exosuit_cost=0)
                construct._build_superproject(p, w, focussed_sp)
        p.workers.tire(w)


class ResearchRemoveParadoxes(CollapsingResearch):
    def _call(self, p):
        super()._call(p)
        if p.paradoxes:
            n = min(2, p.paradoxes)
            print(f'You have {p.paradoxes} paradox(es). '
                  f'Remove how many? [0-{n}]')
            n_rm = int(utils.rep_input([str(i) for i in range(n+1)]))
            for i in range(n_rm):
                p.rm_paradox()

                
class ResearchTwice(CollapsingResearch):
    def _call(self, p):
        w = p.workers.get_active('scientist', 'genius')
        p.pay(exosuits=self.exosuit_cost, water=self.water_cost)
        self._research(p)
        print('You may research once more.')
        self._research(p)
        p.workers.tire(w)
        
