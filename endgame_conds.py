import buildings
import superprojects as sp
import utils

import inspect


class _EndgameCondition:
    def __repr__(self):
        name = self.__class__.__name__
        return name[0].upper() + utils.camel2snake(name[1:]).replace('_', ' ')
    
    def _count(self, p):
        raise NotImplementedError
    
    def points(self, players):
        counts = [self._count(p) for p in players]
        max_count = max(counts)
        return {p.leader: 3*(c==max_count) for p, c in zip(players, counts)}


class MostAdventures(_EndgameCondition):
    def _count(self, p):
        return len(p.adventures)


class MostBreakthroughs(_EndgameCondition):
    def _count(self, p):
        return len(p.breakthroughs)


class MostBuildingSpots(_EndgameCondition):
    def _count(self, p):
        return sum(b is not None for b in p.board)


class HighestJumpRange(_EndgameCondition):
    pp_per_range = {
        1: {101, 112, 113},
        2: {102, 103, 104, 108},
        3: {105, 106, 107, 109, 111, 115},
        4: {110},
        6: {114},
    }
    pp_ranges = {pp_id: j_range for j_range, pp_ids in pp_per_range.items()
                 for pp_id in pp_ids}
    
    def _count(self, p):
        n = 0
        for b in p.board.unique():
            if isinstance(b, buildings.PowerPlant):
                pp_id = int(b.__class__.__name__[-3:])
                n += pp_ranges[pp_id]
            elif isinstance(b, buildings.Lab401):
                n += 1
            elif isinstance(b, buildings.Lab402):
                n += 2
            elif isinstance(b, sp.TemporalTourism):
                n += 3
        return n

    
class HighestMorale(_EndgameCondition):
    def _count(self, p):
        return p.morale - p.morale_lims[1]


class MostSuperprojects(_EndgameCondition):
    def _count(self, p):
        return sum(isinstance(b, sp.Superproject) for b in p.board.unique())


class MostTimeTravels(_EndgameCondition):
    def _count(self, p):
        return p.time_travels
        

class MostWater(_EndgameCondition):
    def _count(self, p):
        return p.resources['water']


class MostWorkers(_EndgameCondition):
    def _count(self, p):
        return p.workers.n()
