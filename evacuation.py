import buildings
from superprojects import Superproject


class _EvacuationCondition:
    def __init__(self, p):
        self._p = p
    
    def __repr__(self, prefix=''):
        return prefix + self._repr().replace('\n', '\n'+prefix)

    def _score_base(self, p):
        raise NotImplementedError

    def _score_extra(self, p):
        raise NotImplementedError
    
    def score(self):
        score_base = self._score_base(self._p)
        vp = score_base + self._score_extra(self._p)
        return min(vp, 30) if score_base else 0

    
class EvacHarmonyA(_EvacuationCondition):
    def _repr(self):
        out  = '(Base) 3 x LifeSupport -> 2 VP\n'
        out += '(Mult) Gold + Genius   -> 3 VP'
        return out

    def _score_base(self, p):
        n = sum(isinstance(b, buildings.LifeSupport)
                for b in p.board.life_supports)
        return 2 if n >= 3 or p._rescue_pods else 0

    def _score_extra(self, p):
        n_gold = p.resources['gold']
        n_geniuses = p.workers.n('genius')
        return min(n_gold, n_geniuses) * 3


class EvacHarmonyB(_EvacuationCondition):
    def _repr(self):
        out  = '(Base) 6 occupied building spots -> 2 VP\n'
        out += '(Mult) Building + Admin          -> 3 VP'
        return out

    def _score_base(self, p):
        n = sum(b is not None for b in p.board)
        return 2 if n >= 6 or p._rescue_pods else 0

    def _score_extra(self, p):
        n_buildings = len(p.board.buildings)
        n_admins = p.workers.n('admin')
        return min(n_buildings, n_admins) * 3


class EvacDominanceA(_EvacuationCondition):
    def _repr(self):
        out  = '(Base) 3 x Factory         -> 5 VP\n'
        out += '(Mult) Titanium + Engineer -> 2 VP'
        return out

    def _score_base(self, p):
        n = sum(isinstance(b, buildings.Factory)
                for b in p.board.factories)
        return 5 if n >= 3 or p._rescue_pods else 0

    def _score_extra(self, p):
        n_titanium = p.resources['titanium']
        n_engineers = p.workers.n('engineer')
        return min(n_titanium, n_engineers) * 2


class EvacDominanceB(_EvacuationCondition):
    def _repr(self):
        out  = '(Base) Maximum morale -> 3 VP\n'
        out += '(Mult) Worker         -> 1 VP'
        return out

    def _score_base(self, p):
        return 3 if p.morale == p.morale_lims[1] or p._rescue_pods else 0

    def _score_extra(self, p):
        return p.workers.n()


class EvacProgressA(_EvacuationCondition):
    def _repr(self):
        out  = '(Base) 3 x Lab                  -> 5 VP\n'
        out += '(Mult) Breakthrough + Scientist -> 2 VP'
        return out

    def _score_base(self, p):
        n = sum(isinstance(b, buildings.Lab) for b in p.board.labs)
        return 5 if n >= 3 or p._rescue_pods else 0

    def _score_extra(self, p):
        n_research = len(p.breakthroughs)
        n_sci = p.workers.n('scientist')
        return min(n_research, n_sci) * 2


class EvacProgressB(_EvacuationCondition):
    def _repr(self):
        out  = '(Base) 8 x Water    -> 3 VP\n'
        out += '(Mult) Superproject -> 4 VP'
        return out

    def _score_base(self, p):
        return 3 if p.resources['water'] >= 8 or p._rescue_pods else 0

    def _score_extra(self, p):
        s = {b for b in p.board if isinstance(b, Superproject)}
        return len(s) * 4


class EvacSalvationA(_EvacuationCondition):
    def _repr(self):
        out  = '(Base) 3 x PowerPlant -> 3 VP\n'
        out += '(Mult) Neutronium     -> 3 VP'
        return out

    def _score_base(self, p):
        n = sum(isinstance(b, buildings.PowerPlant)
                for b in p.board.power_plants)
        return 3 if n >= 3 or p._rescue_pods else 0

    def _score_extra(self, p):
        return p.resources['neutronium'] * 3


class EvacSalvationB(_EvacuationCondition):
    def _repr(self):
        out  = '(Base) 2 x Anomaly         -> 6 VP\n'
        out += '(Mult) Warp tile + Uranium -> 2 VP'
        return out

    def _score_base(self, p):
        n = sum(isinstance(b, buildings.Anomaly) for b in p.board)
        return 6 if n >= 2 or p._rescue_pods else 0

    def _score_extra(self, p):
        n_warps = sum(era is None for wt, era in p.warp_tiles.items())
        n_uranium = p.resources['uranium']
        return min(n_warps, n_uranium) * 2
        
