import numpy as np
import utils
from utils import BREAKTHROUGHS, KEY2WORKER


class AssetCard:
    def __init__(self, resources, number, active_workers='', tired_workers='',
                 breakthroughs=0):
        self.resources = resources
        self.number = number
        self.active_workers = active_workers
        self.tired_workers = tired_workers
        self.breakthroughs = breakthroughs

    def __repr__(self, prefix=''):
        out = utils.longform_res(self.resources, prefix)
        if self.active_workers:
            out += '{}1 x active {}\n'.format(
                prefix, KEY2WORKER[self.active_workers])
        if self.tired_workers:
            out += '{}1 x tired {}\n'.format(
                prefix, KEY2WORKER[self.tired_workers])
        if self.breakthroughs:
            out += '{}{} x random breakthrough\n'.format(
                prefix, self.breakthroughs)
        out += '{}Number: {}\n'.format(prefix, self.number)
        return out

    def receive_assets(self, p):
        p.get(self.resources)
        if self.active_workers:
            p.workers.active[KEY2WORKER[self.active_workers]] += 1
        if self.tired_workers:
            p.workers.tired[KEY2WORKER[self.tired_workers]] += 1
        for i in range(self.breakthroughs):
            p.breakthroughs += [np.random.choice(BREAKTHROUGHS)]
        

asset_cards = [
    AssetCard('g', 1, tired_workers='a'),
    AssetCard('t', 1, tired_workers='e'),
    AssetCard('ww', 2, tired_workers='a'),
    AssetCard('w', 2, active_workers='e'),
    AssetCard('wct', 3),
    AssetCard('wwwtt', 4),
    AssetCard('c', 5, breakthroughs=1),
    AssetCard('w', 5, active_workers='g'),
    AssetCard('g', 6, active_workers='s'),
    AssetCard('u', 6, active_workers='a'),
    AssetCard('u', 6, active_workers='e'),
    AssetCard('ctu', 7),
    AssetCard('cc', 8),
    AssetCard('wttg', 9),
    AssetCard('wttu', 9),
    AssetCard('cn', 10),
]
